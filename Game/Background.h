#pragma once
#include "Helpers.h"
#include <vector>

namespace Law
{
	class Scene;
	class GameObject;
	class Background final
	{
	public:
		Background(Scene* scene, int level, Point2i pos, Point2i dim);

		void Disable();

	private:
		std::vector<GameObject*> m_pObjects;
	};
}