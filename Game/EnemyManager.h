#pragma once
#include "Helpers.h"
#include <vector>

namespace Law
{
	class Scene;
	class Enemy;
	class Digger;
	class EnemyManager
	{
	public:
		EnemyManager(Scene* pScene, Digger* pDigger, Point2i startPos, Point2i dim, const Grid& grid);
		~EnemyManager();

		void PhysicsUpdate();
		void Update();

		void StartLevel(int levelNr);
		void RestartLevel();
		void RemoveEnemy();

	private:
		bool m_LevelStarted;

		int m_ActiveEnemies;
		int m_EnemiesLeft;

		float m_EnemyTimer;
		const float m_TimeBetweenEnemies;

		Point2i m_StartPos;
		Point2i m_Dimensions;

		Grid m_Grid;

		Scene* m_pScene;
		Digger* m_pDigger;

		std::vector<Enemy*> m_pEnemies;
	};
}