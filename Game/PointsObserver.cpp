#include "PointsObserver.h"
#include "Game.h"
#include "Subject.h"

Law::PointsObserver::PointsObserver(Game* pGame)
	: m_HasTakenEmerald{ false }
	, m_Points{ 0 }
	, m_NumEmeralds{ 0 }
	, m_EmeraldsTaken{ 0 }
	, m_EmeraldCtr{ 0.f }
	, m_EmeraldStreakCtr{ 0.5f }
	, m_pGame{ pGame }
{
}

void Law::PointsObserver::Update()
{
	m_EmeraldCtr += Time::GetDeltaTime();

	if (m_EmeraldCtr >= m_EmeraldStreakCtr)
	{
		m_EmeraldCtr = 0.f;
		if (!m_HasTakenEmerald)
		{
			m_EmeraldsTaken = 0;
		}
		m_HasTakenEmerald = false;
	}
}

void Law::PointsObserver::OnNotify(Event event)
{
	int prevPoints = m_Points;
	switch (event)
	{
	case Law::Event::ON_TAKE_EMERALD:
		m_Points += 25;
		m_NumEmeralds--;

		if (m_NumEmeralds == 0)
			m_pGame->NextLevel();

		m_EmeraldsTaken++;

		if (m_EmeraldsTaken == 8)
		{
			m_Points += 250;
			m_EmeraldsTaken = 0;
		}
		m_HasTakenEmerald = true;
		break;

	case Law::Event::ON_TAKE_GOLD:
		m_Points += 500;
		break;
		
	case Law::Event::ON_ENEMY_DIES:
		m_Points += 250;
		break;
		
	case Law::Event::ON_RESTART:
		m_Points = 0;
		break;
	}

	if ((prevPoints < 20000) && (m_Points > 20000))
	{
		m_pSubject->Notify(Event::ON_DIGGER_GAIN_LIFE);
	}
}

void Law::PointsObserver::SetNumEmeralds(int numEmeralds)
{
	m_NumEmeralds = numEmeralds;
}

int Law::PointsObserver::GetPoints() const
{
	return m_Points;
}
