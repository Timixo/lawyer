#pragma once
#include "Helpers.h"

namespace Law
{
	class Scene;
	class GameObject;
	class SpriteComponent;
	class PhysicsComponent;
	class FireBall
	{
	public:
		FireBall(Scene* scene, Point2i dim);

		void PhysicsUpdate();
		void Update();

		void Fire(Point2i pos, MovementState state);
		void Explode();

		bool IsFiring();

	private:
		bool m_IsFiring;
		bool m_IsExploding;

		const float m_MoveSpeed;

		MovementState m_MoveState;

		GameObject* m_pObject;

		SpriteComponent* m_pFireSprite;
		SpriteComponent* m_pExplosionSprite;

		PhysicsComponent* m_pPhysics;
	};
}