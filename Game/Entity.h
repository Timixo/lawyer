#pragma once
#include "Helpers.h"

namespace Law
{
	class Scene;
	class GameObject;
	class Entity
	{
	public:
		Entity(const Grid& grid);
		virtual ~Entity() = default;

		virtual void PhysicsUpdate() {};
		virtual void Update() {};
		virtual void Render() {};

		virtual void Dies() {};

		bool IsDead();
		int GetCurrentGridIdx();
		float GetMovementSpeed();
		GameObject* GetGameObject();

	protected:
		friend class MoveCommand;
		bool m_IsDead;

		int m_CurrentGridIdx;
		int m_NextGridIdx;

		const float m_MovementSpeed;

		Grid m_Grid;

		GameObject* m_pGameObject;

		MovementState m_Direction;
	};
}