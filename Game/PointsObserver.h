#pragma once
#include <Observer.h>

namespace Law
{
	class Game;
	class PointsObserver final : public Observer
	{
	public:
		PointsObserver(Game* pGame);

		void Update();

		virtual void OnNotify(Event event) override;

		void SetNumEmeralds(int numEmeralds);

		int GetPoints() const;

	private:
		bool m_HasTakenEmerald;

		int m_Points;
		int m_NumEmeralds;
		int m_EmeraldsTaken;

		float m_EmeraldCtr;
		const float m_EmeraldStreakCtr;

		Game* m_pGame;
	};
}