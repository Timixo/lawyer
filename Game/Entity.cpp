#include "Entity.h"

Law::Entity::Entity(const Grid& grid)
	: m_IsDead{false}
	, m_CurrentGridIdx{0}
	, m_NextGridIdx{0}
	, m_MovementSpeed{4.f}
	, m_pGameObject{nullptr}
	, m_Grid{grid}
{
}

bool Law::Entity::IsDead()
{
	return m_IsDead;
}

int Law::Entity::GetCurrentGridIdx()
{
	return m_CurrentGridIdx;
}

float Law::Entity::GetMovementSpeed()
{
	return m_MovementSpeed;
}

Law::GameObject* Law::Entity::GetGameObject()
{
	return m_pGameObject;
}
