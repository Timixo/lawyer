#include "Game.h"
#include "Reader.h"
#include "Minigin.h"
#include "Objects.h"
#include "Commands.h"
#include "GameObject.h"
#include "Components.h"
#include "EnemyManager.h"
#include "LifeObserver.h"
#include "PointsObserver.h"
#include "ContactListener.h"
#include <Subject.h>
#include <InputManager.h>
#include <PhysicsManager.h>
#include <AudioManager.h>
#include <SDL.h>

int Law::Game::m_TopBorder{ 50 };
int Law::Game::m_LeftBorder{ 5 };

Law::Game::Game()
	: Scene("Game")
	, m_NeedsRestart{false}
	, m_CurrentLevel{0}
	, m_pDigger{nullptr}
	, m_Grid{}
	, m_pPointsObserver{nullptr}
	, m_pContactListener{nullptr}
{
}

Law::Game::~Game()
{
	for (size_t i{}; i < m_pBags.size(); i++)
	{
		delete m_pBags[i];
	}
	m_pBags.clear();

	for (size_t i{}; i < m_pEmeralds.size(); i++)
	{
		delete m_pEmeralds[i];
	}
	m_pEmeralds.clear();

	for (size_t i{}; i < m_pBackgrounds.size(); i++)
	{
		delete m_pBackgrounds[i];
	}
	m_pBackgrounds.clear();

	for (int row{}; row < m_Grid.rows; row++)
	{
		for (int column{}; column < m_Grid.columns; column++)
		{
			delete m_Grid.grid[row][column];
		}
	}

	delete m_pDigger;
	delete m_pSubject;
	delete m_pEnemyManager;
	delete m_pLifeObserver;
	delete m_pPointsObserver;
	delete m_pContactListener;
}

void Law::Game::Initialize()
{
	m_WindowDim = Minigin::GetWindowDim();

	m_Grid.gamePos.x = m_LeftBorder;
	m_Grid.gamePos.y = m_TopBorder;
	m_Grid.gameDim.x = m_WindowDim.x - 2 * m_LeftBorder;
	m_Grid.gameDim.y = m_WindowDim.y - m_TopBorder;
	
	Reader::GetInstance().Open("Levels/Levels.txt");

	int currentLoading{ 1 };
	int currentLine{ 0 };
	std::string line{};
	line = Reader::GetInstance().ReadLine();
	m_Levels[currentLoading].push_back(std::vector<char>());
	while (line != "EOF")
	{
		if (line == "")
		{
			currentLoading++;
			currentLine = 0;
			m_Levels[currentLoading].push_back(std::vector<char>());
			line = Reader::GetInstance().ReadLine();
			continue;
		}

		for (const char& character : line)
		{
			m_Levels[currentLoading][currentLine].push_back(character);
		}
		currentLine++;
		line = Reader::GetInstance().ReadLine();
		if (line != "" && line != "EOF")
			m_Levels[currentLoading].push_back(std::vector<char>());
	}
	Reader::GetInstance().Close();

	m_Grid.rows = (int)m_Levels[1].size();
	m_Grid.columns = (int)m_Levels[1][0].size();
	m_Grid.cellWidth = m_Grid.gameDim.x / m_Grid.columns;
	m_Grid.cellHeight = m_Grid.gameDim.y / m_Grid.rows;

	m_pBackgrounds.resize(size_t(m_Levels.size() * m_Grid.rows * m_Grid.columns));
	m_pBags.resize(size_t(m_Levels.size() * m_Grid.rows * m_Grid.columns));
	m_pEmeralds.resize(size_t(m_Levels.size() * m_Grid.rows * m_Grid.columns));

	MakeGrid();

	GameObject* pWall = new GameObject(ObjectType::WALL);
	PhysicsComponent* pPhysics = new PhysicsComponent(b2Vec2{ (float)m_Grid.gamePos.x, (float)m_Grid.gamePos.y + (float)m_Grid.gameDim.y }, b2Vec2{ (float)m_Grid.gameDim.x, 5.f }, ObjectType::WALL, 0xFFFF);
	pWall->AddComponent(pPhysics);
	Add(pWall);

	pWall = new GameObject(ObjectType::WALL);
	pPhysics = new PhysicsComponent(b2Vec2{ (float)m_Grid.gamePos.x, (float)m_Grid.gamePos.y - 5.f }, b2Vec2{ (float)m_Grid.gameDim.x, 5.f }, ObjectType::WALL, 0xFFFF);
	pWall->AddComponent(pPhysics);
	Add(pWall);

	pWall = new GameObject(ObjectType::WALL);
	pPhysics = new PhysicsComponent(b2Vec2{ (float)m_Grid.gamePos.x - 5.f, (float)m_Grid.gamePos.y }, b2Vec2{ 5.f, (float)m_Grid.gameDim.y }, ObjectType::WALL, 0xFFFF);
	pWall->AddComponent(pPhysics);
	Add(pWall);

	pWall = new GameObject(ObjectType::WALL);
	pPhysics = new PhysicsComponent(b2Vec2{ (float)m_Grid.gamePos.x + (float)m_Grid.gameDim.x, (float)m_Grid.gamePos.y }, b2Vec2{ 5.f, (float)m_Grid.gameDim.y }, ObjectType::WALL, 0xFFFF);
	pWall->AddComponent(pPhysics);
	Add(pWall);

	GameObject* pPoints = new GameObject(ObjectType::NONE);
	m_pPoints = new TextComponent();
	m_pPoints->SetText("Points: 0");
	m_pPoints->SetFont("Lingua.otf", 50);
	pPoints->AddComponent(m_pPoints);
	Add(pPoints);
	
	m_pSubject = new Subject();
	m_pPointsObserver = new PointsObserver(this);
	m_pLifeObserver = new LifeObserver(this);
	m_pSubject->AddObserver(m_pPointsObserver);
	m_pSubject->AddObserver(m_pLifeObserver);
	
	m_pDigger = new Digger(this, m_pSubject, { m_Grid.gamePos.x + m_Grid.gameDim.x / 2 - m_Grid.cellWidth / 2, m_Grid.gamePos.y + m_Grid.gameDim.y - m_Grid.cellHeight }, { m_Grid.cellWidth, m_Grid.cellHeight }, m_Grid);
	Point2f pos{ m_Grid.grid[m_Grid.rows - 1][m_Grid.columns - 1]->data };
	m_pEnemyManager = new EnemyManager(this, m_pDigger, Point2i{ int(pos.x), int(pos.y) }, Point2i{ m_Grid.cellWidth, m_Grid.cellHeight }, m_Grid);
	m_pContactListener = new ContactListener(m_pSubject, m_pEnemyManager);
	PhysicsManager::GetInstance().GetWorld()->SetContactListener(m_pContactListener);
	
	GameObject* pStart = new GameObject(ObjectType::NONE);
	m_pStartScreen = new TextureComponent();
	m_pStartScreen->SetTexture("Sprites/Title.png", Texture::TITLE, 1);
	m_pStartScreen->SetDimensions({ m_WindowDim.x, m_WindowDim.y });
	m_pStartScreen->SetShow(true);
	pStart->AddComponent(m_pStartScreen);
	Add(pStart);

	m_pStartCommand = new StartCommand();
	m_pStartCommand->AddStartScreen(m_pStartScreen);
	m_pStartCommand->AddGame(this);

	InputStruct pStartStruct(0, InputTriggerState::PRESSED, 0, m_pStartCommand, VK_RETURN, XINPUT_GAMEPAD_START);
	InputManager::GetInstance().AddInput(pStartStruct);

	for (int i{}; i < m_pLifeObserver->GetLives(); i++)
	{
		GameObject* pLife = new GameObject(ObjectType::NONE);
		SpriteComponent* pLifeText = new SpriteComponent();
		SpriteSheetInfo info{};
		info.rows = 4;
		info.columns = 6;
		info.spriteSheet[SpriteState::MOVINGDOWN] = std::make_pair(0, 6);
		info.spriteSheet[SpriteState::MOVINGLEFT] = std::make_pair(1, 6);
		info.spriteSheet[SpriteState::MOVINGRIGHT] = std::make_pair(2, 6);
		info.spriteSheet[SpriteState::MOVINGUP] = std::make_pair(3, 6);
		pLifeText->SetSpriteSheet("Sprites/DiggingSheet.png", Texture::PLAYER, info, 2);
		pLifeText->SetSpriteState(SpriteState::MOVINGRIGHT);
		pLifeText->SetDimensions({60, 60});
		pLifeText->SetShow(false);
		pLifeText->SetSpriteCol(0);
		pLifeText->Pause();
		m_pLifeTextures.push_back(pLifeText);

		pLife->AddComponent(pLifeText);

		pLife->GetTransform()->SetPosition(m_WindowDim.x / 2 - 20 + (i * 50.f), 0.f, 0.f);

		Add(pLife);
	}

	AudioManager::GetInstance().AddChannel(Channel::BACKGROUND);
	AudioManager::GetInstance().AddSound("Sound/PopCorn.mp3", Sound::BACKGROUND, FMOD_LOOP_NORMAL);
	AudioManager::GetInstance().StartSound(Sound::BACKGROUND, Channel::BACKGROUND);
	AudioManager::GetInstance().SetVolume(0.1f, Channel::BACKGROUND);
}

void Law::Game::PhysicsUpdate()
{
	if (m_CurrentLevel == 0)
		return;

	m_pDigger->PhysicsUpdate();

	m_pEnemyManager->PhysicsUpdate();

	for (Bag* pBag : m_pBags)
	{
		if (pBag)
			pBag->PhysicsUpdate();
	}
}

void Law::Game::Update()
{
	if (m_CurrentLevel == 0)
	{
		for (TextureComponent* pText : m_pLifeTextures)
			pText->SetShow(false);
		return;
	}

	m_pDigger->Update();

	if (m_NeedsRestart)
	{
		GenerateLevel(m_CurrentLevel);
		m_NeedsRestart = false;
		return;
	}
	m_pEnemyManager->Update();
	for (Bag* pBag : m_pBags)
	{
		if (pBag)
			pBag->Update();
	}

	m_pPointsObserver->Update();
	m_pPoints->SetText("Points: " + std::to_string(m_pPointsObserver->GetPoints()));

	for (size_t i{}; i < m_pLifeTextures.size(); i++)
	{
		if (i < size_t(m_pLifeObserver->GetLives()))
			m_pLifeTextures[i]->SetShow(true);
		else m_pLifeTextures[i]->SetShow(false);
	}
}

void Law::Game::Render() const
{
}

void Law::Game::NextLevel()
{
	m_CurrentLevel++;

	m_NeedsRestart = (m_CurrentLevel <= int(m_Levels.size()));

	for (size_t i{}; i < m_pBackgrounds.size(); i++)
	{
		if (m_pBackgrounds[i])
		{
			m_pBackgrounds[i]->Disable();
			delete m_pBackgrounds[i];
			m_pBackgrounds[i] = nullptr;
		}
		if (m_pBags[i])
		{
			m_pBags[i]->Disable();
			delete m_pBags[i];
			m_pBags[i] = nullptr;
		}
		if (m_pEmeralds[i])
		{
			m_pEmeralds[i]->Disable();
			delete m_pEmeralds[i];
			m_pEmeralds[i] = nullptr;
		}
	}

	for (size_t i{}; i < m_pLifeTextures.size(); i++)
	{
		if (i < size_t(m_pLifeObserver->GetLives()))
			m_pLifeTextures[i]->SetShow(true);
		else m_pLifeTextures[i]->SetShow(false);
	}

	m_pDigger->Restart();
	m_pEnemyManager->StartLevel(m_CurrentLevel);
}

void Law::Game::CleanLevel()
{
	for (Bag* pBag : m_pBags)
	{
		if (pBag)
		{
			if (pBag->GetState() != BagState::SUSPENDED)
				pBag->Disable();
		}
	}
	m_pEnemyManager->RestartLevel();
}

void Law::Game::GameOver()
{
	m_pStartScreen->SetShow(true);
	m_CurrentLevel = 0;

	for (size_t i{}; i < m_pBackgrounds.size(); i++)
	{
		if (m_pBags[i])
		{
			m_pBags[i]->Disable();
			delete m_pBags[i];
			m_pBags[i] = nullptr;
		}
		if (m_pBackgrounds[i])
		{
			m_pBackgrounds[i]->Disable();
			delete m_pBackgrounds[i];
			m_pBackgrounds[i] = nullptr;
		}
		if (m_pEmeralds[i])
		{
			m_pEmeralds[i]->Disable();
			delete m_pEmeralds[i];
			m_pEmeralds[i] = nullptr;
		}
	}
	m_pDigger->Restart();

	m_pSubject->Notify(Event::ON_RESTART);

	for (TextureComponent* pText : m_pLifeTextures)
		pText->SetShow(false);

	m_pStartCommand->Activate(true);
}

void Law::Game::MakeGrid()
{
	Node* pNode{ nullptr };
	for (int row{}; row < m_Grid.rows; row++)
	{
		m_Grid.grid.push_back(std::vector<Node*>());
		for (int column{}; column < m_Grid.columns; column++)
		{
			pNode = new Node();
			pNode->data = { float(m_Grid.gamePos.x + (column * m_Grid.cellWidth)), float(m_Grid.gamePos.y + m_Grid.gameDim.y - ((row + 1) * m_Grid.cellHeight)) };
			m_Grid.grid[row].push_back(pNode);

			if (row != 0)
			{
				Node* pTopNode = m_Grid.grid[row - 1][column];
				pNode->pTop = pTopNode;
				pTopNode->pDown = pNode;
			}

			if (column != 0)
			{
				Node* pLeftNode = m_Grid.grid[row][column - 1];
				pNode->pLeft = pLeftNode;
				pLeftNode->pRight = pNode;
			}
		}
	}
}

void Law::Game::GenerateLevel(int levelNr)
{
	int numBags{ 0 };
	int numEmeralds{ 0 };
	int numBackgrounds{ 0 };
	std::vector<std::pair<Point2i, int>> bags;
	for (int row{}; row < m_Grid.rows; row++)
	{
		for (int col{}; col < m_Grid.columns; col++)
		{
			switch (m_Levels[levelNr][row][col])
			{
			case 'B':
				m_pBackgrounds[numBackgrounds] = new Background(this, m_CurrentLevel, { m_Grid.gamePos.x + col * m_Grid.cellWidth, m_Grid.gamePos.y + (row * m_Grid.cellHeight) }, { m_Grid.cellWidth, m_Grid.cellHeight });
				numBackgrounds++;

				bags.push_back(std::make_pair(Point2i{ m_Grid.gamePos.x + col * m_Grid.cellWidth, m_Grid.gamePos.y + (row * m_Grid.cellHeight) }, numBackgrounds - 1));
				break;
			case 'C':
				m_pBackgrounds[numBackgrounds] = new Background(this, m_CurrentLevel, { m_Grid.gamePos.x + col * m_Grid.cellWidth, m_Grid.gamePos.y + (row * m_Grid.cellHeight) }, { m_Grid.cellWidth, m_Grid.cellHeight });
				numBackgrounds++;

				m_pEmeralds[numEmeralds] = new Emerald(this, { m_Grid.gamePos.x + col * m_Grid.cellWidth, m_Grid.gamePos.y + (row * m_Grid.cellHeight) }, { m_Grid.cellWidth, m_Grid.cellHeight });
				numEmeralds++;
				break;
			case ' ':
				m_pBackgrounds[numBackgrounds] = new Background(this, m_CurrentLevel, { m_Grid.gamePos.x + col * m_Grid.cellWidth, m_Grid.gamePos.y + (row * m_Grid.cellHeight) }, { m_Grid.cellWidth, m_Grid.cellHeight });
				numBackgrounds++;
				break;
			}
		}
	}

	for (const std::pair<Point2i, int> bag : bags)
	{
		m_pBags[numBags] = new Bag(this, bag.first, { m_Grid.cellWidth, m_Grid.cellHeight }, m_pBackgrounds[bag.second], m_Grid);
		numBags++;
	}

	m_pPointsObserver->SetNumEmeralds(numEmeralds);
}