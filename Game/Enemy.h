#pragma once
#include "Entity.h"

namespace Law
{
	class Digger;
	class SpriteComponent;
	class PhysicsComponent;
	enum class SideCollisions
	{
		TOP = 0,
		RIGHT = 1,
		DOWN = 2,
		LEFT = 3
	};

	class Scene;
	class Enemy final : public Entity
	{
	public:
		Enemy(Scene* scene, Digger* pDigger, Point2i pos, Point2i dim, const Grid& grid);

		void PhysicsUpdate() override;
		void Update() override;
		virtual void Dies() override;

		void Disable();
		void Collides(bool isColliding, SideCollisions side);

	private:
		int m_FramesStuck;
		int m_IsColliding[4];

		Point2f m_OldPos;

		Digger* m_pDigger;

		SpriteComponent* m_pNobbinSprite;
		PhysicsComponent* m_pPhysics;

		void CheckNextDirection();
		void CheckBranchDirection();
	};
}