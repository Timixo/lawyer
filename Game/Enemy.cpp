#include "Enemy.h"
#include "Digger.h"
#include <Scene.h>
#include <GameObject.h>
#include <Components.h>

Law::Enemy::Enemy(Scene* scene, Digger* pDigger, Point2i pos, Point2i dim, const Grid& grid)
	: Entity(grid)
	, m_pDigger{pDigger}
	, m_IsColliding{}
{
	m_pGameObject = new GameObject(ObjectType::ENEMY);
	m_pGameObject->SetUserData(this);

	m_Direction = MovementState::LEFT;

	m_CurrentGridIdx = m_Grid.GetIdxFromCoord(Point2f{ float(pos.x), float(pos.y) });
	m_NextGridIdx = m_CurrentGridIdx - 1;

	m_pNobbinSprite = new SpriteComponent();
	SpriteSheetInfo nobbinInfo{};
	nobbinInfo.rows = 1;
	nobbinInfo.columns = 4;
	nobbinInfo.spriteSheet[SpriteState::MOVINGLEFT] = std::make_pair(0, 4);
	m_pNobbinSprite->SetSpriteSheet("Sprites/NobbinSheet.png", Texture::NOBBIN, nobbinInfo, 2);
	m_pNobbinSprite->SetSpriteState(SpriteState::MOVINGLEFT);
	m_pNobbinSprite->SetDimensions(dim);
	m_pNobbinSprite->SetShow(true);
	m_pNobbinSprite->SetFrameTime(1.f / 12.f);
	m_pGameObject->AddComponent(m_pNobbinSprite);

	m_pPhysics = new PhysicsComponent(b2Vec2{ float(pos.x + 3), float(pos.y + 4) }, b2Vec2{ float(dim.x * 6 / 7), float(dim.y * 3 / 4) }
		, ObjectType::ENEMY, ObjectType::DIGGER | ObjectType::WALL | ObjectType::BACKGROUND | ObjectType::BAG | ObjectType::FIREBALL, BodyShape::CIRCLE, false, false, true);
	m_pGameObject->AddComponent(m_pPhysics);

	m_pPhysics->AddFixture(b2Vec2{ 0.f, float(-dim.y / 2) }, b2Vec2{ float(dim.x / 2), float(dim.y) }, ObjectType::ENEMYTOP, ObjectType::BACKGROUND | ObjectType::WALL, BodyShape::RECTANGLE);
	m_pPhysics->AddFixture(b2Vec2{ float(dim.x / 2), 0.f }, b2Vec2{ float(dim.x), float(dim.y / 2) }, ObjectType::ENEMYRIGHT, ObjectType::BACKGROUND | ObjectType::WALL, BodyShape::RECTANGLE);
	m_pPhysics->AddFixture(b2Vec2{ 0.f, float(dim.y / 2) }, b2Vec2{ float(dim.x / 2), float(dim.y) }, ObjectType::ENEMYDOWN, ObjectType::BACKGROUND | ObjectType::WALL, BodyShape::RECTANGLE);
	m_pPhysics->AddFixture(b2Vec2{ float(-dim.x / 2), 0.f }, b2Vec2{ float(dim.x), float(dim.y / 2) }, ObjectType::ENEMYLEFT, ObjectType::BACKGROUND | ObjectType::WALL, BodyShape::RECTANGLE);

	scene->Add(m_pGameObject);
}

void Law::Enemy::PhysicsUpdate()
{
	if (m_IsDead)
		return;

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0, m_MovementSpeed });
		break;
	case Law::MovementState::LEFT:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ -m_MovementSpeed, 0 });
		break;
	case Law::MovementState::RIGHT:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ m_MovementSpeed, 0 });
		break;
	case Law::MovementState::UP:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0, -m_MovementSpeed });
		break;
	default:
		break;
	}
}

void Law::Enemy::Update()
{
	if (m_IsDead)
		return;

	glm::vec3 tempPos{ m_pGameObject->GetTransform()->GetPosition() };
	Point2f diggerPos{ tempPos.x, tempPos.y };
	Point2f goalPos{ m_Grid.GetCoordFromIdx(m_NextGridIdx) };

	if (m_OldPos == diggerPos)
	{
		m_FramesStuck++;
		if (m_FramesStuck >= 10)
		{
			CheckNextDirection();
		}
		return;
	}

	m_OldPos = diggerPos;

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		if (diggerPos.y >= (goalPos.y))
		{
			m_CurrentGridIdx = m_NextGridIdx;
			if (m_IsColliding[int(SideCollisions::DOWN)] > 0)
				CheckNextDirection();
			else if (m_IsColliding[int(SideCollisions::LEFT)] == 0 || m_IsColliding[int(SideCollisions::RIGHT)] == 0)
				CheckBranchDirection();
			else
				m_NextGridIdx -= m_Grid.columns;
		}
		break;
	case Law::MovementState::LEFT:
		if (diggerPos.x <= (goalPos.x + 8.f))
		{
			m_CurrentGridIdx = m_NextGridIdx;
			if (m_IsColliding[int(SideCollisions::LEFT)] > 0)
				CheckNextDirection();
			else if (m_IsColliding[int(SideCollisions::TOP)] == 0 || m_IsColliding[int(SideCollisions::DOWN)] == 0)
				CheckBranchDirection();
			else
				m_NextGridIdx -= 1;
		}
		break;
	case Law::MovementState::RIGHT:
		if (diggerPos.x >= (goalPos.x))
		{
			m_CurrentGridIdx = m_NextGridIdx;
			if (m_IsColliding[int(SideCollisions::RIGHT)] > 0)
				CheckNextDirection();
			else if (m_IsColliding[int(SideCollisions::TOP)] == 0 || m_IsColliding[int(SideCollisions::DOWN)] == 0)
				CheckBranchDirection();
			else
				m_NextGridIdx += 1;
		}
		break;
	case Law::MovementState::UP:
		if (diggerPos.y <= (goalPos.y + 5.f))
		{
			m_CurrentGridIdx = m_NextGridIdx;
			if (m_IsColliding[int(SideCollisions::TOP)] > 0)
				CheckNextDirection();
			else if (m_IsColliding[int(SideCollisions::LEFT)] == 0 || m_IsColliding[int(SideCollisions::RIGHT)] == 0)
				CheckBranchDirection();
			else
				m_NextGridIdx += m_Grid.columns;
		}
		break;
	}
}

void Law::Enemy::Dies()
{
	m_IsDead = true;
	m_pNobbinSprite->Pause();
	m_pNobbinSprite->SetShow(false);
}

void Law::Enemy::Disable()
{
	m_pGameObject->Disable();
}

void Law::Enemy::Collides(bool isColliding, SideCollisions side)
{
	if (isColliding) m_IsColliding[int(side)]++;
	else m_IsColliding[int(side)]--;
}

void Law::Enemy::CheckNextDirection()
{
	glm::vec3 deltaPosVec{ m_pDigger->GetGameObject()->GetTransform()->GetPosition() - m_pGameObject->GetTransform()->GetPosition() };
	Point2f deltaPos{ deltaPosVec.x, deltaPosVec.y };

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		if (deltaPos.x < 0)
		{
			if (m_IsColliding[int(SideCollisions::LEFT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else if (m_IsColliding[int(SideCollisions::RIGHT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
		}
		else
		{
			if (m_IsColliding[int(SideCollisions::RIGHT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
			else if (m_IsColliding[int(SideCollisions::LEFT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
		}
		break;
	case Law::MovementState::LEFT:
		if (deltaPos.y < 0)
		{
			if (m_IsColliding[int(SideCollisions::TOP)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else if (m_IsColliding[int(SideCollisions::DOWN)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
		}
		else
		{
			if (m_IsColliding[int(SideCollisions::DOWN)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
			else if (m_IsColliding[int(SideCollisions::TOP)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
		}
		break;
	case Law::MovementState::RIGHT:
		if (deltaPos.y < 0)
		{
			if (m_IsColliding[int(SideCollisions::TOP)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else if (m_IsColliding[int(SideCollisions::DOWN)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
		}
		else
		{
			if (m_IsColliding[int(SideCollisions::DOWN)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
			else if (m_IsColliding[int(SideCollisions::TOP)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
		}
		break;
	case Law::MovementState::UP:
		if (deltaPos.x < 0)
		{
			if (m_IsColliding[int(SideCollisions::LEFT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else if (m_IsColliding[int(SideCollisions::RIGHT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
		}
		else
		{
			if (m_IsColliding[int(SideCollisions::RIGHT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
			else if (m_IsColliding[int(SideCollisions::LEFT)] == 0)
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
		}
		break;
	default:
		break;
	}
}

void Law::Enemy::CheckBranchDirection()
{
	glm::vec3 deltaPosVec{ m_pDigger->GetGameObject()->GetTransform()->GetPosition() - m_pGameObject->GetTransform()->GetPosition() };
	Point2f deltaPos{ deltaPosVec.x, deltaPosVec.y };

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		if (deltaPos.y > m_Grid.cellHeight)
		{
			if ((abs(deltaPos.x) > m_Grid.cellWidth) && (abs(deltaPos.x) < abs(deltaPos.y)))
			{
				if (deltaPos.x < 0 && (m_IsColliding[int(SideCollisions::LEFT)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx - 1;
					m_Direction = MovementState::LEFT;
				}
				else if (deltaPos.x > 0 && (m_IsColliding[int(SideCollisions::RIGHT)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx + 1;
					m_Direction = MovementState::RIGHT;
				}
			}
		}
		else
		{
			if (deltaPos.x < 0 && (m_IsColliding[int(SideCollisions::LEFT)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else if (deltaPos.x > 0 && (m_IsColliding[int(SideCollisions::RIGHT)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
		}
		break;
	case Law::MovementState::LEFT:
		if (deltaPos.x < -m_Grid.cellWidth)
		{
			if ((abs(deltaPos.y) > m_Grid.cellHeight) && (abs(deltaPos.y) < abs(deltaPos.x)))
			{
				if (deltaPos.y < 0 && (m_IsColliding[int(SideCollisions::TOP)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
					m_Direction = MovementState::UP;
				}
				else if (deltaPos.y > 0 && (m_IsColliding[int(SideCollisions::DOWN)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
					m_Direction = MovementState::DOWN;
				}
			}
		}
		else
		{
			if (deltaPos.y < 0 && (m_IsColliding[int(SideCollisions::TOP)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else if (deltaPos.y > 0 && (m_IsColliding[int(SideCollisions::DOWN)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
		}
		break;
	case Law::MovementState::RIGHT:
		if (deltaPos.x > m_Grid.cellWidth)
		{
			if ((abs(deltaPos.y) > m_Grid.cellHeight) && (abs(deltaPos.y) < abs(deltaPos.x)))
			{
				if (deltaPos.y < 0 && (m_IsColliding[int(SideCollisions::TOP)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
					m_Direction = MovementState::UP;
				}
				else if (deltaPos.y > 0 && (m_IsColliding[int(SideCollisions::DOWN)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
					m_Direction = MovementState::DOWN;
				}
			}
		}
		else
		{
			if (deltaPos.y < 0 && (m_IsColliding[int(SideCollisions::TOP)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx + m_Grid.columns;
				m_Direction = MovementState::UP;
			}
			else if (deltaPos.y > 0 && (m_IsColliding[int(SideCollisions::DOWN)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx - m_Grid.columns;
				m_Direction = MovementState::DOWN;
			}
		}
		break;
	case Law::MovementState::UP:
		if (deltaPos.y < -m_Grid.cellHeight)
		{
			if ((abs(deltaPos.x) > m_Grid.cellWidth) && (abs(deltaPos.x) < abs(deltaPos.y)))
			{
				if (deltaPos.x < 0 && (m_IsColliding[int(SideCollisions::LEFT)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx - 1;
					m_Direction = MovementState::LEFT;
				}
				else if (deltaPos.x > 0 && (m_IsColliding[int(SideCollisions::RIGHT)] == 0))
				{
					m_NextGridIdx = m_CurrentGridIdx + 1;
					m_Direction = MovementState::RIGHT;
				}
			}
		}
		else
		{
			if (deltaPos.x < 0 && (m_IsColliding[int(SideCollisions::LEFT)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx - 1;
				m_Direction = MovementState::LEFT;
			}
			else if (deltaPos.x > 0 && (m_IsColliding[int(SideCollisions::RIGHT)] == 0))
			{
				m_NextGridIdx = m_CurrentGridIdx + 1;
				m_Direction = MovementState::RIGHT;
			}
		}
		break;
	}
}
