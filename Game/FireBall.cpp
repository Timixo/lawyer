#include "FireBall.h"
#include <Scene.h>
#include <GameObject.h>
#include <Components.h>

Law::FireBall::FireBall(Scene* scene, Point2i dim)
	: m_IsFiring{false}
	, m_IsExploding{false}
	, m_MoveSpeed{3.f}
{
	m_pObject = new GameObject(ObjectType::FIREBALL);
	m_pObject->SetUserData(this);

	m_pFireSprite = new SpriteComponent();
	SpriteSheetInfo fireInfo{};
	fireInfo.rows = 1;
	fireInfo.columns = 6;
	fireInfo.spriteSheet[SpriteState::FIRING] = std::make_pair(0, 6);
	m_pFireSprite->SetSpriteSheet("Sprites/FireSheet.png", Texture::FIRE, fireInfo, 2);
	m_pFireSprite->SetSpriteState(SpriteState::FIRING);

	m_pFireSprite->SetDimensions({ dim.x, dim.y });
	m_pFireSprite->Pause();
	m_pFireSprite->SetShow(false);
	m_pObject->AddComponent(m_pFireSprite);

	m_pExplosionSprite = new SpriteComponent();
	SpriteSheetInfo explosionInfo{};
	explosionInfo.rows = 1;
	explosionInfo.columns = 6;
	explosionInfo.spriteSheet[SpriteState::FIRING] = std::make_pair(0, 6);
	m_pExplosionSprite->SetSpriteSheet("Sprites/FireSheet.png", Texture::EXPLOSION, explosionInfo, 2);
	m_pExplosionSprite->SetSpriteState(SpriteState::FIRING);

	m_pExplosionSprite->SetDimensions({ dim.x, dim.y });
	m_pExplosionSprite->Pause();
	m_pExplosionSprite->SetShow(false);
	m_pExplosionSprite->SetLooping(false, true);
	m_pObject->AddComponent(m_pExplosionSprite);

	m_pPhysics = new PhysicsComponent(b2Vec2{}, b2Vec2{ float(dim.x), float(dim.y) }, ObjectType::FIREBALL, ObjectType::BACKGROUND | ObjectType::ENEMY, BodyShape::CIRCLE, false, false, true);
	m_pObject->AddComponent(m_pPhysics);

	scene->Add(m_pObject);
}

void Law::FireBall::PhysicsUpdate()
{
	if (m_IsFiring)
	{
		switch (m_MoveState)
		{
		case Law::MovementState::DOWN:
			m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0, m_MoveSpeed });
			break;
		case Law::MovementState::LEFT:
			m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ -m_MoveSpeed, 0 });
			break;
		case Law::MovementState::RIGHT:
			m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ m_MoveSpeed, 0 });
			break;
		case Law::MovementState::UP:
			m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0, -m_MoveSpeed });
			break;
		}
	}
	else
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0.f, 0.f });
}

void Law::FireBall::Update()
{

}

void Law::FireBall::Fire(Point2i pos, MovementState state)
{
	m_IsFiring = true;
	m_pPhysics->SetPosition(Point2f{float(pos.x), float(pos.y)});
	m_MoveState = state;
	m_pFireSprite->SetShow(true);
	m_pFireSprite->Play();
}

void Law::FireBall::Explode()
{
	m_IsFiring = false;
	m_IsExploding = true;
	m_pFireSprite->Pause();
	m_pFireSprite->SetShow(false);
	m_pExplosionSprite->SetShow(true);
	m_pExplosionSprite->Play();
}

bool Law::FireBall::IsFiring()
{
	return m_IsFiring;
}
