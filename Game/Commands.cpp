#include "Commands.h"
#include "Components.h"
#include "Time.h"
#include "Logger.h"
#include "Entity.h"
#include "Digger.h"
#include "Game.h"
#include <GameObject.h>
#include <iostream>

Law::ControllerCommand::ControllerCommand()
{
	for (int i{}; i < XUSER_MAX_COUNT; i++)
	{
		m_pPlayerPhysics[i] = nullptr;
	}
}

void Law::ControllerCommand::AddPlayer(int playerIdx, Component* component)
{
	if (playerIdx < XUSER_MAX_COUNT)
	{
		PhysicsComponent* pPhysics{ dynamic_cast<PhysicsComponent*>(component) };
		if (pPhysics)
		{
			m_pPlayerPhysics[playerIdx] = pPhysics;
			return;
		}
		Logger::LogWarning("Command::ControllerCommand::AddPlayer >> PhysicsComponent at playerIndex is not valid");
	}
	Logger::LogWarning("Command::ControllerCommand::AddPlayer >> Playerindex given is out of range");
}

Law::MoveCommand::MoveCommand(MovementState movement, const Grid& grid)
	: m_MoveState{movement}
	, m_Grid{grid}
{
}

void Law::MoveCommand::Execute(int playerIdx)
{
	if (m_Owner->m_IsDead)
		return;

	if (playerIdx < XUSER_MAX_COUNT)
	{
		PhysicsComponent* pPhysics = m_pPlayerPhysics[playerIdx];
		if (pPhysics)
		{
			b2Body* pBody{ pPhysics->GetBody() };
			b2Vec2 linVel{ pBody->GetLinearVelocity() };

			int currentGridIdx{ m_Owner->m_CurrentGridIdx };
			int nextGridIdx{ m_Owner->m_NextGridIdx };

			if (currentGridIdx == nextGridIdx)
			{
				int nextIdx{};
				switch (m_MoveState)
				{
				case Law::MovementState::DOWN:
					nextIdx = currentGridIdx - m_Grid.columns;
					m_Owner->m_Direction = MovementState::DOWN;
					if (nextIdx < 0) 
						nextIdx = currentGridIdx;
					break;
				case Law::MovementState::LEFT:
					nextIdx = currentGridIdx - 1;
					m_Owner->m_Direction = MovementState::LEFT;
					if (((nextIdx / m_Grid.columns) < (currentGridIdx / m_Grid.columns)) || (nextIdx < 0)) 
						nextIdx = currentGridIdx;
					break;
				case Law::MovementState::RIGHT:
					nextIdx = currentGridIdx + 1;
					m_Owner->m_Direction = MovementState::RIGHT;
					if ((nextIdx / m_Grid.columns) > (currentGridIdx / m_Grid.columns)) 
						nextIdx = currentGridIdx;
					break;
				case Law::MovementState::UP:
					Digger* pDigger = static_cast<Digger*>(m_Owner->GetGameObject()->GetUserData());
					if (pDigger && !pDigger->CanMoveUp())
						return;
					nextIdx = currentGridIdx + m_Grid.columns;
					m_Owner->m_Direction = MovementState::UP;
					if (nextIdx >= (m_Grid.rows * m_Grid.columns)) 
						nextIdx = currentGridIdx;
					break;
				}
				m_Owner->m_NextGridIdx = nextIdx;
			}
			else
			{
				switch (m_Owner->m_Direction)
				{
				case Law::MovementState::DOWN:
					linVel = b2Vec2{ 0, m_Owner->GetMovementSpeed() };
					break;
				case Law::MovementState::LEFT:
					linVel = b2Vec2{ -m_Owner->GetMovementSpeed(), 0 };
					break;
				case Law::MovementState::RIGHT:
					linVel = b2Vec2{ m_Owner->GetMovementSpeed(), 0 };
					break;
				case Law::MovementState::UP:
					linVel = b2Vec2{ 0, -m_Owner->GetMovementSpeed() };
					break;
				default:
					break;
				}
			}

			pBody->SetLinearVelocity(linVel);

			return;
		}
		Logger::LogWarning("Command::MoveCommand::Execute >> PhysicsComponent at playerIndex is not valid");
	}
	Logger::LogWarning("Command::MoveCommand::Execute >> Playerindex given is out of range");
}

void Law::MoveCommand::AddOwner(Entity* pOwner)
{
	m_Owner = pOwner;
	m_pTransform = pOwner->GetGameObject()->GetTransform();
	glm::vec3 pos{ m_pTransform->GetPosition() };
	int idx = m_Grid.GetIdxFromCoord(Point2f{ pos.r, pos.g });
	m_Owner->m_CurrentGridIdx = idx;
	m_Owner->m_NextGridIdx = idx;
}

void Law::FireCommand::Execute(int playerIdx)
{
	if (playerIdx < XUSER_MAX_COUNT)
	{
		Digger* pDigger = static_cast<Digger*>(m_Owner);
		pDigger->Fire();
	}
}

void Law::FireCommand::AddOwner(Entity* pOwner)
{
	m_Owner = pOwner;
}

Law::StartCommand::StartCommand()
	: m_IsActivated{true}
{
}

void Law::StartCommand::Activate(bool activate)
{
	m_IsActivated = activate;
}

void Law::StartCommand::AddGame(Game* pGame)
{
	m_pGame = pGame;
}

void Law::StartCommand::AddStartScreen(TextureComponent* pStartScreen)
{
	m_pStartScreen = pStartScreen;
}

void Law::StartCommand::Execute(int playerIdx)
{
	UNREFERENCED_PARAMETER(playerIdx);

	if (m_IsActivated)
	{
		m_pStartScreen->SetShow(false);
		m_pGame->NextLevel();
		Activate(false);
	}
}

void Law::StartCommand::AddPlayer(int playerIdx, Component* component)
{
	UNREFERENCED_PARAMETER(playerIdx);
	UNREFERENCED_PARAMETER(component);
}
