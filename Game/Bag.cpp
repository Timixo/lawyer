#include "Bag.h"
#include "Background.h"
#include <Time.h>
#include <Scene.h>
#include <GameObject.h>
#include <Components.h>
#include <Logger.h>
#include <ResourceManager.h>
#include <algorithm>

Law::Bag::Bag(Scene* scene, Point2i pos, Point2i dim, Background* pBackground, const Grid& grid)
	: m_StoppingBlocks{0}
	, m_CurrentIdx{}
	, m_LevelsFallen{}
	, m_TrembleTimer{0.f}
	, m_BurstTime{0.f}
	, m_MaxTrembleTime{1.f}
	, m_MaxBurstTime{1.f}
	, m_MoveSpeed{5.f}
	, m_Grid{grid}
	, m_pBackground{pBackground}
	, m_State{BagState::SUSPENDED}
	, m_Position{pos}
	, m_pObjToDestroy{}
{
	m_pObject = new GameObject(ObjectType::BAG);
	m_pObject->SetUserData(this);

	m_CurrentIdx = m_Grid.GetIdxFromCoord(pos);

	m_BagSprite = new SpriteComponent();
	SpriteSheetInfo bagInfo{};
	bagInfo.rows = 1;
	bagInfo.columns = 4;
	bagInfo.spriteSheet[SpriteState::MOVINGDOWN] = std::make_pair(0, 4);
	m_BagSprite->SetSpriteSheet("Sprites/MoneyBagSheet.png", Texture::BAG, bagInfo, 2);
	m_BagSprite->SetSpriteState(SpriteState::MOVINGDOWN);

	m_BagSprite->SetDimensions({ dim.x, dim.y });
	m_BagSprite->Pause();
	m_BagSprite->SetShow(true);
	m_pObject->AddComponent(m_BagSprite);

	m_CoinsSprite = new SpriteComponent();
	SpriteSheetInfo coinsInfo{};
	coinsInfo.rows = 1;
	coinsInfo.columns = 3;
	coinsInfo.spriteSheet[SpriteState::MOVINGDOWN] = std::make_pair(0, 3);
	m_CoinsSprite->SetSpriteSheet("Sprites/GoldSheet.png", Texture::COINS, coinsInfo, 2);
	m_CoinsSprite->SetSpriteState(SpriteState::MOVINGDOWN);

	m_CoinsSprite->SetDimensions({ dim.x, dim.y });
	m_CoinsSprite->Pause();
	m_CoinsSprite->SetShow(false);
	m_CoinsSprite->SetLooping(false, false);
	m_pObject->AddComponent(m_CoinsSprite);

	m_pPhysics = new PhysicsComponent(b2Vec2{ float(pos.x + 2.f), float(pos.y + 2.f) }, b2Vec2{ float(dim.x * 2 / 3), float(dim.y * 2 / 3) }
	, ObjectType::BAG, ObjectType::DIGGER | ObjectType::WALL | ObjectType::BACKGROUND | ObjectType::ENEMY, BodyShape::CIRCLE, true, false, true);
	m_pPhysics->SetDynamic();
	m_pObject->AddComponent(m_pPhysics);

	m_pPhysics->AddFixture(b2Vec2{ 0.f, float(dim.y) }, b2Vec2{ float(dim.x / 2), float(dim.y / 2) }, ObjectType::BAGSENSOR
		, ObjectType::DIGGER | ObjectType::BACKGROUND | ObjectType::WALL);

	m_pPhysics->AddFixture(b2Vec2{ 2.f, float(dim.y / 2) }, b2Vec2{ float(dim.x * 2 / 3), float(dim.y / 2) }
	, ObjectType::BAGBREAKER, ObjectType::DIGGER | ObjectType::BACKGROUND | ObjectType::WALL, BodyShape::RECTANGLE);

	m_pPhysics->AddFixture(b2Vec2{ -float(dim.x / 4), 0.f }, b2Vec2{ float(dim.x / 3), float(dim.y / 3) }, ObjectType::BAGLEFT
		, ObjectType::DIGGER, BodyShape::RECTANGLE);

	m_pPhysics->AddFixture(b2Vec2{ float(dim.x / 4), 0.f }, b2Vec2{ float(dim.x / 3), float(dim.y / 3) }, ObjectType::BAGRIGHT
		, ObjectType::DIGGER, BodyShape::RECTANGLE);

	scene->Add(m_pObject);
}

void Law::Bag::PhysicsUpdate()
{
	if (m_State == BagState::SUSPENDED)
		return;

	switch (m_State)
	{
	case Law::BagState::FALLING:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ 0.f, m_MoveSpeed });
		break;
	case Law::BagState::PUSHEDLEFT:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ m_MoveSpeed, 0.f });
		break;
	case Law::BagState::PUSHEDRIGHT:
		m_pPhysics->GetBody()->SetLinearVelocity(b2Vec2{ -m_MoveSpeed, 0.f });
		break;
	}
	m_CanBeStopped = (m_StoppingBlocks <= 2);
}

void Law::Bag::Update()
{
	if (m_State == BagState::SUSPENDED)
		return;

	switch (m_State)
	{
	case Law::BagState::TREMBLING:
		m_TrembleTimer += Time::GetDeltaTime();

		if (m_TrembleTimer >= m_MaxTrembleTime)
		{
			for (size_t i{}; i < m_pObjToDestroy.size(); i++)
			{
				m_pObjToDestroy[i]->Disable();
			}
			m_State = BagState::FALLING;
			m_TrembleTimer = 0;
		}
		break;
	case Law::BagState::PUSHEDLEFT:
	{
		glm::vec3 vecPos = m_pObject->GetTransform()->GetPosition();
		Point2f pos{ vecPos.x, vecPos.y };
		Point2f nextIdxCoord{ m_Grid.GetCoordFromIdx(m_GoalIdx) };

		if (pos.x >= (nextIdxCoord.x + 2.f))
		{
			if (!m_CanBeStopped)
			{
				m_State = BagState::SUSPENDED;
				m_CurrentIdx = m_GoalIdx;
			}
			else
			{
				m_State = BagState::TREMBLING;
				m_TrembleTimer = m_MaxTrembleTime;
				m_CurrentIdx = m_GoalIdx;
			}
		}
	}
		break;
	case Law::BagState::PUSHEDRIGHT:
	{
		glm::vec3 vecPos = m_pObject->GetTransform()->GetPosition();
		Point2f pos{ vecPos.x, vecPos.y };
		Point2f nextIdxCoord{ m_Grid.GetCoordFromIdx(m_GoalIdx) };

		if (pos.x <= (nextIdxCoord.x + 2.f))
		{
			if (!m_CanBeStopped)
			{
				m_State = BagState::SUSPENDED;
				m_CurrentIdx = m_GoalIdx;
			}
			else
			{
				m_State = BagState::TREMBLING;
				m_TrembleTimer = m_MaxTrembleTime;
				m_CurrentIdx = m_GoalIdx;
			}
		}
	}
		break;
	case Law::BagState::BURST:
		break;
	}
}

void Law::Bag::UnderMined()
{
	m_State = BagState::TREMBLING;
	m_BagSprite->Play();
}

void Law::Bag::Disable()
{
	m_pObjToDestroy.clear();
	m_pObject->Disable();
}

void Law::Bag::AddStoppingBlock()
{
	m_StoppingBlocks++;
}

void Law::Bag::SubStoppingBlock()
{
	m_StoppingBlocks--;
}

void Law::Bag::TouchesGround(bool forcedStop)
{
	glm::vec3 tranPos = m_pObject->GetTransform()->GetPosition();
	Point2f pos{ tranPos.x + 2, tranPos.y};

	int newIdx{ m_Grid.GetIdxFromCoord(pos) };

	if (m_StoppingBlocks >= 2 || forcedStop)
	{
		if (newIdx < m_CurrentIdx - m_Grid.columns)
		{
			m_State = BagState::BURST;
			m_BagSprite->Pause();
			m_BagSprite->SetShow(false);
			m_CoinsSprite->SetShow(true);
			m_CoinsSprite->Play();
		}
		else
		{
			m_State = BagState::SUSPENDED;
			m_BagSprite->Pause();
		}
	}
	else
	{
		for (size_t i{}; i < m_pObjToDestroy.size(); i++)
		{
			m_pObjToDestroy[i]->Disable();
		}
	}
}

void Law::Bag::AddToDestroy(GameObject* pObj)
{
	m_pObjToDestroy.push_back(pObj);
}

void Law::Bag::DeleteFromDestroy(GameObject* pObj)
{
	for (size_t i{}; i < m_pObjToDestroy.size(); i++)
	{
		if (pObj && m_pObjToDestroy[i])
		{
			if (pObj == m_pObjToDestroy[i])
			{
				m_pObjToDestroy[i] = m_pObjToDestroy[m_pObjToDestroy.size() - 1];
				m_pObjToDestroy.pop_back();
			}
		}
	}
}

void Law::Bag::Take()
{
	Disable();
}

void Law::Bag::Push(bool fromLeft)
{
	m_State = (fromLeft ? BagState::PUSHEDLEFT : BagState::PUSHEDRIGHT);
	m_GoalIdx = (fromLeft ? m_CurrentIdx + 1 : m_CurrentIdx - 1);
}

Law::BagState Law::Bag::GetState()
{
	return m_State;
}
