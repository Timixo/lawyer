#pragma once
#include "Helpers.h"
#include <vector>

namespace Law
{
	enum class BagState
	{
		SUSPENDED = 0,
		TREMBLING = 1,
		FALLING = 2,
		PUSHEDLEFT = 4,
		PUSHEDRIGHT = 5,
		BURST = 6
	};

	class Scene;
	class GameObject;
	class Background;
	class SpriteComponent;
	class PhysicsComponent;
	class Bag final
	{
	public:
		Bag(Scene* scene, Point2i pos, Point2i dim, Background* pBackground, const Grid& grid);

		void PhysicsUpdate();
		void Update();

		void UnderMined();
		void Disable();
		void AddStoppingBlock();
		void SubStoppingBlock();
		void TouchesGround(bool forcedStop);
		void AddToDestroy(GameObject* pObj);
		void DeleteFromDestroy(GameObject* pObj);
		void Take();
		void Push(bool fromLeft);

		BagState GetState();

	private:
		bool m_CanBeStopped;

		int m_StoppingBlocks;
		int m_CurrentIdx;
		int m_GoalIdx;
		int m_LevelsFallen;

		float m_TrembleTimer;
		float m_BurstTime;
		const float m_MaxTrembleTime;
		const float m_MaxBurstTime;
		const float m_MoveSpeed;

		Point2i m_Position;

		Grid m_Grid;

		BagState m_State;

		GameObject* m_pObject;

		Background* m_pBackground;

		SpriteComponent* m_BagSprite;
		SpriteComponent* m_CoinsSprite;

		PhysicsComponent* m_pPhysics;

		std::vector<GameObject*> m_pObjToDestroy;
	};
}