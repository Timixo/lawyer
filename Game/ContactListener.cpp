#include "ContactListener.h"
#include "Objects.h"
#include "EnemyManager.h"
#include <Subject.h>
#include <Components.h>
#include <GameObject.h>
#include <Logger.h>
#include <Box2D.h>

Law::ContactListener::ContactListener(Subject* pSubject, EnemyManager* pEnemyManager)
	: m_pSubject{pSubject}
	, m_pEnemyManager{pEnemyManager}
{
}

void Law::ContactListener::BeginContact(b2Contact* contact)
{
	b2Fixture* pFixture1 = contact->GetFixtureA();
	b2Fixture* pFixture2 = contact->GetFixtureB();

	void* bodyUserData1 = pFixture1->GetBody()->GetUserData();
	void* bodyUserData2 = pFixture2->GetBody()->GetUserData();

	if (bodyUserData1 && bodyUserData2)
	{
		PhysicsComponent* pPhysics1 = static_cast<PhysicsComponent*>(bodyUserData1);
		PhysicsComponent* pPhysics2 = static_cast<PhysicsComponent*>(bodyUserData2);

		GameObject* pObject1 = pPhysics1->GetGameObject();
		GameObject* pObject2 = pPhysics2->GetGameObject();

		ObjectType type1 = pObject1->GetObjectType();
		ObjectType type2 = pObject2->GetObjectType();

		Enemy* pEnemy;
		Digger* pDigger;
		Emerald* pEmerald;
		Bag* pBag;
		FireBall* pFireBall;
		BagState bagState;
		uint16 categoryBits;

		switch (type1)
		{
		case Law::ObjectType::DIGGER:
			pDigger = static_cast<Digger*>(pObject1->GetUserData());
			switch (type2)
			{
			case Law::BACKGROUND:
				pObject2->Disable();
				break;
			case Law::BAG:
				pBag = static_cast<Bag*>(pObject2->GetUserData());
				bagState = pBag->GetState();
				categoryBits = pFixture2->GetFilterData().categoryBits;
				switch (categoryBits)
				{
				case ObjectType::BAGSENSOR:
					if (bagState == Law::BagState::SUSPENDED)
					{
						pBag->UnderMined();
					}
					break;
				case ObjectType::BAGBREAKER:
					pDigger->HitsBag(true);
					break;
				case ObjectType::BAGLEFT:
					if (pBag->GetState() == BagState::SUSPENDED)
					{
						pDigger->MakeInvincible();
						pBag->Push(true);
					}
					break;
				case ObjectType::BAGRIGHT:
					if (pBag->GetState() == BagState::SUSPENDED)
					{
						pDigger->MakeInvincible();
						pBag->Push(false);
					}
					break;
				case ObjectType::BAG:
					switch (bagState)
					{
					case Law::BagState::SUSPENDED:
						break;
					case Law::BagState::TREMBLING:
						break;
					case Law::BagState::FALLING:
						if (!pDigger->IsInvincible())
						{
							pDigger->Dies();
						}
						break;
					case Law::BagState::PUSHEDLEFT:
						break;
					case Law::BagState::PUSHEDRIGHT:
						break;
					case Law::BagState::BURST:
						m_pSubject->Notify(Event::ON_TAKE_GOLD);
						pBag->Take();
						break;
					default:
						break;
					}
					break;
				}
				break;
			case Law::EMERALD:
				pEmerald = static_cast<Emerald*>(pObject2->GetUserData());
				if (pEmerald)
				{
					pEmerald->Take();
					m_pSubject->Notify(Event::ON_TAKE_EMERALD);
				}
				break;
			case Law::ENEMY:
				if (!static_cast<Enemy*>(pObject2->GetUserData())->IsDead())
				{
					pDigger->Dies();
				}
				break;
			}
			break;


		case Law::ObjectType::BACKGROUND:
			switch (type2)
			{
			case Law::DIGGER:
				pObject1->Disable();
				break;
			case Law::BAG:
				pBag = static_cast<Bag*>(pObject2->GetUserData());
				categoryBits = pFixture2->GetFilterData().categoryBits;
				switch (categoryBits)
				{
				case ObjectType::BAGBREAKER:
					pBag->AddToDestroy(pObject1);
					break;
				case ObjectType::BAGSENSOR:
					pBag->AddStoppingBlock();
					break;
				case ObjectType::BAG:
					pBag->AddToDestroy(pObject1);
					if (pBag->GetState() == BagState::FALLING)
						pBag->TouchesGround(false);
					break;
				}
				break;
			case Law::FIREBALL:
				pFireBall = static_cast<FireBall*>(pObject2->GetUserData());
				pFireBall->Explode();
				break;
			case Law::ENEMY:
				categoryBits = pFixture2->GetFilterData().categoryBits;
				pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
				switch (categoryBits)
				{
				case ObjectType::ENEMYTOP:
					pEnemy->Collides(true, SideCollisions::TOP);
					break;
				case ObjectType::ENEMYRIGHT:
					pEnemy->Collides(true, SideCollisions::RIGHT);
					break;
				case ObjectType::ENEMYDOWN:
					pEnemy->Collides(true, SideCollisions::DOWN);
					break;
				case ObjectType::ENEMYLEFT:
					pEnemy->Collides(true, SideCollisions::LEFT);
					break;
				}
				break;
			}
			break;


		case Law::ObjectType::BAG:
			pBag = static_cast<Bag*>(pObject1->GetUserData());
			categoryBits = pFixture1->GetFilterData().categoryBits;
			bagState = pBag->GetState();
			switch (categoryBits)
			{
			case ObjectType::BAGSENSOR:
				switch (type2)
				{
				case Law::DIGGER:
					if (bagState == Law::BagState::SUSPENDED)
					{
						pBag->UnderMined();
					}
					break;
				case Law::BACKGROUND:
					pBag->AddStoppingBlock();
					break;
				case Law::WALL:
					break;
				}
				break;
			case ObjectType::BAGBREAKER:
				if (type2 == ObjectType::BACKGROUND)
				{
					pBag->AddToDestroy(pObject2);
				}
				if (type2 == ObjectType::DIGGER)
				{
					pDigger = static_cast<Digger*>(pObject2->GetUserData());
					pDigger->HitsBag(true);
				}
				break;
			case ObjectType::BAGLEFT:
				if (type2 == ObjectType::DIGGER || type2 == ObjectType::BAG)
				{
					if (pBag->GetState() == BagState::SUSPENDED)
					{
						static_cast<Digger*>(pObject2->GetUserData())->MakeInvincible();
						pBag->Push(true);
					}
				}
				break;
			case ObjectType::BAGRIGHT:
				if (type2 == ObjectType::DIGGER || type2 == ObjectType::BAG)
				{
					if (pBag->GetState() == BagState::SUSPENDED)
					{
						static_cast<Digger*>(pObject2->GetUserData())->MakeInvincible();
						pBag->Push(false);
					}
				}
				break;
			case ObjectType::BAG:
				switch (type2)
				{
				case Law::NONE:
					break;
				case Law::DIGGER:
					switch (bagState)
					{
					case Law::BagState::FALLING:
						pDigger = static_cast<Digger*>(pObject2->GetUserData());
						if (!pDigger->IsInvincible())
						{
							pDigger->Dies();
						}
						break;
					case Law::BagState::BURST:
						m_pSubject->Notify(Event::ON_TAKE_GOLD);
						static_cast<Bag*>(pObject1->GetUserData())->Take();
						break;
					default:
						break;
					}
					break;
				case Law::BACKGROUND:
					pBag->AddToDestroy(pObject2);
					if (pBag->GetState() == BagState::FALLING)
						pBag->TouchesGround(false);
					break;
				case Law::EMERALD:
					break;
				case Law::WALL:
					if (pBag->GetState() == BagState::FALLING)
						pBag->TouchesGround(true);
					break;
				case Law::ENEMY:
					if (pBag->GetState() == BagState::FALLING)
					{
						pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
						m_pEnemyManager->RemoveEnemy();
						pEnemy->Dies();
						m_pSubject->Notify(Event::ON_ENEMY_DIES);
					}
					else if (pBag->GetState() == BagState::BURST)
						pBag->Disable();
					break;
				}
				break;
			}
			break;


		case Law::ObjectType::EMERALD:
			if (type2 == ObjectType::DIGGER)
			{
				pEmerald = static_cast<Emerald*>(pObject1->GetUserData());
				if (pEmerald)
				{
					pEmerald->Take();
					m_pSubject->Notify(Event::ON_TAKE_EMERALD);
				}
			}
			break;

		case Law::ObjectType::FIREBALL:
			pFireBall = static_cast<FireBall*>(pObject1->GetUserData());
			switch (type2)
			{
			case Law::BACKGROUND:
				pFireBall->Explode();
				break;
			case Law::ENEMY:
				pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
				if (pFireBall->IsFiring() && !pEnemy->IsDead())
				{
					pFireBall->Explode();
					m_pEnemyManager->RemoveEnemy();
					pEnemy->Dies();
					m_pSubject->Notify(Event::ON_ENEMY_DIES);
				}
				break;
			}
			break;

		case Law::ObjectType::WALL:
			switch (type2)
			{

			case Law::BAG:
				if (pFixture2->GetFilterData().categoryBits == ObjectType::BAG)
				{
					pBag = static_cast<Bag*>(pObject2->GetUserData());
					if (pBag->GetState() == BagState::FALLING)
						pBag->TouchesGround(true);
				}
				break;
			case Law::ENEMY:
				categoryBits = pFixture2->GetFilterData().categoryBits;
				pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
				switch (categoryBits)
				{
				case ObjectType::ENEMYTOP:
					pEnemy->Collides(true, SideCollisions::TOP);
					break;
				case ObjectType::ENEMYRIGHT:
					pEnemy->Collides(true, SideCollisions::RIGHT);
					break;
				case ObjectType::ENEMYDOWN:
					pEnemy->Collides(true, SideCollisions::DOWN);
					break;
				case ObjectType::ENEMYLEFT:
					pEnemy->Collides(true, SideCollisions::LEFT);
					break;
				}
				break;
			}
			break;

		case Law::ObjectType::ENEMY:
			categoryBits = pFixture1->GetFilterData().categoryBits;
			pEnemy = static_cast<Enemy*>(pObject1->GetUserData());
			switch (type2)
			{
			case Law::DIGGER:
				if (!pEnemy->IsDead())
				{
					pDigger = static_cast<Digger*>(pObject2->GetUserData());
					pDigger->Dies();
				}
				break;
			case Law::BAG:
				pBag = static_cast<Bag*>(pObject2->GetUserData());
				if (pBag->GetState() == BagState::FALLING)
				{
					m_pEnemyManager->RemoveEnemy();
					pEnemy->Dies();
					m_pSubject->Notify(Event::ON_ENEMY_DIES);
				}
				else if (pBag->GetState() == BagState::BURST)
					pBag->Disable();
				break;
			case Law::FIREBALL:
				pFireBall = static_cast<FireBall*>(pObject2->GetUserData());
				if (pFireBall->IsFiring() && !pEnemy->IsDead())
				{
					pFireBall->Explode();
					m_pEnemyManager->RemoveEnemy();
					pEnemy->Dies();
					m_pSubject->Notify(Event::ON_ENEMY_DIES);
				}
				break;
			default:
				switch (categoryBits)
				{
				case ObjectType::ENEMYTOP:
					pEnemy->Collides(true, SideCollisions::TOP);
					break;
				case ObjectType::ENEMYRIGHT:
					pEnemy->Collides(true, SideCollisions::RIGHT);
					break;
				case ObjectType::ENEMYDOWN:
					pEnemy->Collides(true, SideCollisions::DOWN);
					break;
				case ObjectType::ENEMYLEFT:
					pEnemy->Collides(true, SideCollisions::LEFT);
					break;
				}
			break;
			}
		}
	}
}

void Law::ContactListener::EndContact(b2Contact* contact)
{
	b2Fixture* pFixture1 = contact->GetFixtureA();
	b2Fixture* pFixture2 = contact->GetFixtureB();

	void* bodyUserData1 = pFixture1->GetBody()->GetUserData();
	void* bodyUserData2 = pFixture2->GetBody()->GetUserData();

	if (bodyUserData1 && bodyUserData2)
	{
		PhysicsComponent* pPhysics1 = static_cast<PhysicsComponent*>(bodyUserData1);
		PhysicsComponent* pPhysics2 = static_cast<PhysicsComponent*>(bodyUserData2);

		GameObject* pObject1 = pPhysics1->GetGameObject();
		GameObject* pObject2 = pPhysics2->GetGameObject();

		ObjectType type1 = pObject1->GetObjectType();
		ObjectType type2 = pObject2->GetObjectType();

		uint16 categoryBits;
		Bag* pBag;
		Digger* pDigger;
		Enemy* pEnemy;

		switch (type1)
		{
		case Law::BACKGROUND:
			switch (type2)
			{
			case Law::BAG:
				pBag = static_cast<Bag*>(pObject2->GetUserData());
				categoryBits = pFixture2->GetFilterData().categoryBits;
				switch (categoryBits)
				{
				case ObjectType::BAGBREAKER:
				case ObjectType::BAG:
					pBag->DeleteFromDestroy(pObject1);
					break;
				case ObjectType::BAGSENSOR:
					pBag->SubStoppingBlock();
					break;
				}
				break;
			case Law::ENEMY:
				categoryBits = pFixture2->GetFilterData().categoryBits;
				pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
				switch (categoryBits)
				{
				case ObjectType::ENEMYTOP:
					pEnemy->Collides(false, SideCollisions::TOP);
					break;
				case ObjectType::ENEMYRIGHT:
					pEnemy->Collides(false, SideCollisions::RIGHT);
					break;
				case ObjectType::ENEMYDOWN:
					pEnemy->Collides(false, SideCollisions::DOWN);
					break;
				case ObjectType::ENEMYLEFT:
					pEnemy->Collides(false, SideCollisions::LEFT);
					break;
				}
				break;
			}
			break;

		case Law::DIGGER:
			if (type2 == ObjectType::BAG)
			{
				if (pFixture2->GetFilterData().categoryBits == ObjectType::BAGBREAKER)
				{
					pDigger = static_cast<Digger*>(pObject1->GetUserData());
					pDigger->HitsBag(false);
				}
			}
			break;

		case Law::BAG:
			categoryBits = pFixture1->GetFilterData().categoryBits;
			if (type2 == ObjectType::BACKGROUND)
			{
				pBag = static_cast<Bag*>(pObject1->GetUserData());
				categoryBits = pFixture2->GetFilterData().categoryBits;
				switch (categoryBits)
				{
				case ObjectType::BAGBREAKER:
				case ObjectType::BAG:
					pBag->DeleteFromDestroy(pObject2);
					break;
				case ObjectType::BAGSENSOR:
					pBag->SubStoppingBlock();
					break;
				}
			}
			if (type2 == ObjectType::DIGGER)
			{
				if (categoryBits == ObjectType::BAGBREAKER)
				{
					pDigger = static_cast<Digger*>(pObject2->GetUserData());
					pDigger->HitsBag(false);
				}
			}
			break;

		case Law::ENEMY:
			categoryBits = pFixture1->GetFilterData().categoryBits;
			pEnemy = static_cast<Enemy*>(pObject1->GetUserData());
			switch (categoryBits)
			{
			case ObjectType::ENEMYTOP:
				pEnemy->Collides(false, SideCollisions::TOP);
				break;
			case ObjectType::ENEMYRIGHT:
				pEnemy->Collides(false, SideCollisions::RIGHT);
				break;
			case ObjectType::ENEMYDOWN:
				pEnemy->Collides(false, SideCollisions::DOWN);
				break;
			case ObjectType::ENEMYLEFT:
				pEnemy->Collides(false, SideCollisions::LEFT);
				break;
			}
			break;

		case Law::WALL:
			categoryBits = pFixture2->GetFilterData().categoryBits;
			pEnemy = static_cast<Enemy*>(pObject2->GetUserData());
			switch (categoryBits)
			{
			case ObjectType::ENEMYTOP:
				pEnemy->Collides(false, SideCollisions::TOP);
				break;
			case ObjectType::ENEMYRIGHT:
				pEnemy->Collides(false, SideCollisions::RIGHT);
				break;
			case ObjectType::ENEMYDOWN:
				pEnemy->Collides(false, SideCollisions::DOWN);
				break;
			case ObjectType::ENEMYLEFT:
				pEnemy->Collides(false, SideCollisions::LEFT);
				break;
			}
			break;
		}
	}
}
