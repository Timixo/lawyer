#pragma once
#include "..\3rdParty\Box2D\include\box2d\b2_world_callbacks.h"

namespace Law
{
	class Subject;
	class EnemyManager;
	class ContactListener final : public b2ContactListener
	{
	public:
		ContactListener(Subject* pSubject, EnemyManager* pEnemyManager);

	private:
		virtual void BeginContact(b2Contact* contact);
		virtual void EndContact(b2Contact* contact);
		
		Subject* m_pSubject;
		EnemyManager* m_pEnemyManager;
	};
}