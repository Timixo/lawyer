#include "Digger.h"
#include "Game.h"
#include "Commands.h"
#include "FireBall.h"
#include <Scene.h>
#include <Subject.h>
#include <GameObject.h>
#include <Components.h>
#include <InputManager.h>
#include <ResourceManager.h>
#include <Logger.h>

Law::Digger::Digger(Scene* scene, Subject* pSubject, Point2i pos, Point2i dim, const Grid& grid)
	: Entity(grid)
	, m_CanFire{true}
	, m_NeedsRestart{false}
	, m_CanMoveUp{true}
	, m_IsInvincible{false}
	, m_FireTimer{0.f}
	, m_DiedTimer{0.f}
	, m_InvincibleTimer{0.f}
	, m_FireRate{5.f}
	, m_TimeDead{4.f}
	, m_TimeInvincible{1.f}
	, m_StartPos{pos}
	, m_Dimensions{dim}
	, m_pScene{scene}
	, m_pFireball{nullptr}
	, m_pSprite{nullptr}
	, m_pSubject{pSubject}
{
	m_Direction = MovementState::RIGHT;
	m_pGameObject = new GameObject(ObjectType::DIGGER);
	m_pGameObject->SetUserData(this);

	m_pSprite = new SpriteComponent();
	SpriteSheetInfo info{};
	info.rows = 4;
	info.columns = 6;
	info.spriteSheet[SpriteState::MOVINGDOWN] = std::make_pair(0, 6);
	info.spriteSheet[SpriteState::MOVINGLEFT] = std::make_pair(1, 6);
	info.spriteSheet[SpriteState::MOVINGRIGHT] = std::make_pair(2, 6);
	info.spriteSheet[SpriteState::MOVINGUP] = std::make_pair(3, 6);
	m_pSprite->SetSpriteSheet("Sprites/DiggingSheet.png", Texture::PLAYER, info, 2);
	m_pSprite->SetSpriteState(SpriteState::MOVINGRIGHT);
	m_pSprite->SetDimensions(dim);
	m_pSprite->SetShow(true);
	m_pGameObject->AddComponent(m_pSprite);

	m_pDiedSprite = new SpriteComponent();
	SpriteSheetInfo diedInfo{};
	diedInfo.rows = 1;
	diedInfo.columns = 5;
	diedInfo.spriteSheet[SpriteState::MOVINGUP] = std::make_pair(0, 5);
	m_pDiedSprite->SetSpriteSheet("Sprites/GraveSheet.png", Texture::GRAVE, diedInfo, 2);
	m_pDiedSprite->SetSpriteState(SpriteState::MOVINGUP);
	m_pDiedSprite->SetDimensions(dim);
	m_pDiedSprite->SetShow(false);
	m_pDiedSprite->Pause();
	m_pDiedSprite->SetLooping(false, false);
	m_pDiedSprite->SetFrameTime(1.f / 2.f);
	m_pGameObject->AddComponent(m_pDiedSprite);

	m_pGameObject->GetTransform()->SetPosition((float)pos.x, (float)pos.y, 0.5f);

	m_pPhysics = new PhysicsComponent(b2Vec2{ (float)pos.x, (float)pos.y }, b2Vec2((float)dim.x - 8.f, (float)dim.y - 8.f), ObjectType::DIGGER
		, (ObjectType::BACKGROUND | ObjectType::BAG | ObjectType::BAGSENSOR | ObjectType::BAGBREAKER | ObjectType::BAGLEFT | ObjectType::BAGRIGHT | ObjectType::EMERALD | ObjectType::ENEMY)
		, BodyShape::CIRCLE, false, false, true);
	m_pGameObject->AddComponent(m_pPhysics);
	
	scene->Add(m_pGameObject);

	MoveCommand* moveDown = new MoveCommand(MovementState::DOWN, grid);
	moveDown->AddOwner(this);
	moveDown->AddPlayer(0, m_pPhysics);

	MoveCommand* moveLeft = new MoveCommand(MovementState::LEFT, grid);
	moveLeft->AddOwner(this);
	moveLeft->AddPlayer(0, m_pPhysics);

	MoveCommand* moveRight = new MoveCommand(MovementState::RIGHT, grid);
	moveRight->AddOwner(this);
	moveRight->AddPlayer(0, m_pPhysics);

	MoveCommand* moveUp = new MoveCommand(MovementState::UP, grid);
	moveUp->AddOwner(this);
	moveUp->AddPlayer(0, m_pPhysics);

	FireCommand* fire = new FireCommand();
	fire->AddOwner(this);
	fire->AddPlayer(0, m_pPhysics);

	InputStruct inputMoveDown(10, Law::InputTriggerState::DOWN, 0, moveDown, 'S', XINPUT_GAMEPAD_DPAD_DOWN);
	InputManager::GetInstance().AddInput(inputMoveDown);

	InputStruct inputMoveLeft(11, Law::InputTriggerState::DOWN, 0, moveLeft, 'A', XINPUT_GAMEPAD_DPAD_LEFT);
	InputManager::GetInstance().AddInput(inputMoveLeft);

	InputStruct inputMoveRight(12, Law::InputTriggerState::DOWN, 0, moveRight, 'D', XINPUT_GAMEPAD_DPAD_RIGHT);
	InputManager::GetInstance().AddInput(inputMoveRight);

	InputStruct inputMoveUp(13, Law::InputTriggerState::DOWN, 0, moveUp, 'W', XINPUT_GAMEPAD_DPAD_UP);
	InputManager::GetInstance().AddInput(inputMoveUp);

	InputStruct inputFire(14, Law::InputTriggerState::DOWN, 0, fire, VK_SPACE, XINPUT_GAMEPAD_A);
	InputManager::GetInstance().AddInput(inputFire);

	m_pFireball = new FireBall(scene, { dim.x / 2, dim.y / 2 });
}

Law::Digger::~Digger()
{
	delete m_pFireball;
}

void Law::Digger::PhysicsUpdate()
{
	m_pFireball->PhysicsUpdate();
}

void Law::Digger::Update()
{
	if (m_IsDead)
	{
		m_DiedTimer += Time::GetDeltaTime();
		if (m_DiedTimer >= m_TimeDead)
		{
			static_cast<Game*>(m_pScene)->CleanLevel();
			m_DiedTimer = 0;
			m_IsDead = false;
			m_pDiedSprite->SetShow(false);
			m_pSprite->SetShow(true);
			m_pSprite->Play();
			m_NeedsRestart = true;
			m_pSubject->Notify(Event::ON_DIGGER_DIES);
		}
		return;
	}

	if (m_IsInvincible)
	{
		m_InvincibleTimer += Time::GetDeltaTime();
		if (m_InvincibleTimer >= m_TimeInvincible)
		{
			m_IsInvincible = false;
			m_InvincibleTimer = 0.f;
		}
	}

	if (m_NeedsRestart)
	{
		m_pPhysics->SetPosition({ float(m_StartPos.x + m_Dimensions.x / 2.f), float(m_StartPos.y + m_Dimensions.y / 2.f) });
		m_NeedsRestart = false;
		m_CurrentGridIdx = m_Grid.GetIdxFromCoord(Point2f{ float(m_StartPos.x + m_Dimensions.x / 2.f), float(m_StartPos.y + m_Dimensions.y / 2.f) });
		m_NextGridIdx = m_CurrentGridIdx;
		return;
	}

	glm::vec3 tempPos{ m_pGameObject->GetTransform()->GetPosition() };
	Point2f diggerPos{ tempPos.x, tempPos.y };
	Point2f goalPos{ m_Grid.GetCoordFromIdx(m_NextGridIdx) };

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		if (diggerPos.y >= (goalPos.y))
		{
			m_CurrentGridIdx = m_NextGridIdx;
		}
		m_pSprite->SetSpriteState(SpriteState::MOVINGDOWN);
		break;
	case Law::MovementState::LEFT:
		if (diggerPos.x <= (goalPos.x + 8.f))
		{
			m_CurrentGridIdx = m_NextGridIdx;
		}
		m_pSprite->SetSpriteState(SpriteState::MOVINGLEFT);
		break;
	case Law::MovementState::RIGHT:
		if (diggerPos.x >= (goalPos.x))
		{
			m_CurrentGridIdx = m_NextGridIdx;
		}
		m_pSprite->SetSpriteState(SpriteState::MOVINGRIGHT);
		break;
	case Law::MovementState::UP:
		if (diggerPos.y <= (goalPos.y + 5.f))
		{
			m_CurrentGridIdx = m_NextGridIdx;
		}
		m_pSprite->SetSpriteState(SpriteState::MOVINGUP);
		break;
	}

	if (!m_CanFire)
	{
		m_FireTimer += Time::GetDeltaTime();

		if (m_FireTimer >= m_FireRate)
		{
			m_CanFire = true;
			m_FireTimer = 0.f;
		}
	}
}

void Law::Digger::Fire()
{
	if (!m_CanFire)
		return;

	if (m_pFireball->IsFiring())
	{
		m_CanFire = false;
		return;
	}

	glm::vec3 objPos = m_pGameObject->GetTransform()->GetPosition();
	Point2i pos{ int(objPos.x), int(objPos.y) };

	switch (m_Direction)
	{
	case Law::MovementState::DOWN:
		pos.x += m_Dimensions.x / 2;
		pos.y += m_Dimensions.y;
		break;
	case Law::MovementState::LEFT:
		pos.y += m_Dimensions.y / 2;
		break;
	case Law::MovementState::RIGHT:
		pos.x += m_Dimensions.x;
		pos.y += m_Dimensions.y / 2;
		break;
	case Law::MovementState::UP:
		pos.x += m_Dimensions.x / 2;
		break;
	}

	m_pFireball->Fire(pos, m_Direction);
}

void Law::Digger::Restart()
{
	m_NeedsRestart = true;
}

void Law::Digger::Dies()
{
	m_IsDead = true;
	m_pSprite->Pause();
	m_pSprite->SetShow(false);
	m_pDiedSprite->SetShow(true);
	m_pDiedSprite->Play();
}

void Law::Digger::HitsBag(bool hits)
{
	m_CanMoveUp = !hits;
}

void Law::Digger::MakeInvincible()
{
	m_IsInvincible = true;
}

bool Law::Digger::CanMoveUp()
{
	return m_CanMoveUp;
}

bool Law::Digger::IsInvincible()
{
	return m_IsInvincible;
}
