#if _DEBUG
// ReSharper disable once CppUnusedIncludeDirective
#include <vld.h>
#endif

#ifndef _USE_MATH_DEFINES
    #define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include "Minigin.h"
#include "SceneManager.h"  
#include "Game.h"
#include <SDL.h>
#include <Box2D.h>
#include <iostream>
#include <Windows.h>

void AddScenes();

int main(int, char*[]) 
{
    try
    {
        Law::Minigin engine{};
        AddScenes();
        engine.Initialize();
        engine.Run();
    }
    catch (const std::runtime_error & e)
    {
        MessageBox(NULL, e.what(), nullptr, MB_CANCELTRYCONTINUE);
    }
    return 0;
}

void AddScenes()
{
    Law::SceneManager::GetInstance().AddScene(new Law::Game());
}