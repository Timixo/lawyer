#pragma once
#include "Helpers.h"

namespace Law
{
	class Scene;
	class GameObject;
	class Emerald final
	{
	public:
		Emerald(Scene* scene, Point2i pos, Point2i dim);

		void Take();
		void Disable();

	private:
		GameObject* m_pObject;
	};
}