#include "EnemyManager.h"
#include "Digger.h"
#include "Enemy.h"
#include <Scene.h>
#include <Time.h>
#include <algorithm>

Law::EnemyManager::EnemyManager(Scene* pScene, Digger* pDigger, Point2i startPos, Point2i dim, const Grid& grid)
	: m_LevelStarted{false}
	, m_ActiveEnemies{0}
	, m_EnemiesLeft{0}
	, m_EnemyTimer{0.f}
	, m_TimeBetweenEnemies{5.f}
	, m_StartPos{startPos}
	, m_Dimensions{dim}
	, m_Grid{grid}
	, m_pScene{pScene}
	, m_pDigger{pDigger}
{
}

Law::EnemyManager::~EnemyManager()
{
	for (size_t i{}; i < m_pEnemies.size(); i++)
	{
		delete m_pEnemies[i];
	}
	m_pEnemies.clear();
}

void Law::EnemyManager::PhysicsUpdate()
{
	for (Enemy* pEnemy : m_pEnemies)
	{
		if (pEnemy)
			pEnemy->PhysicsUpdate();
	}
}

void Law::EnemyManager::Update()
{
	m_EnemyTimer += Time::GetDeltaTime();

	if (m_EnemyTimer >= m_TimeBetweenEnemies)
	{
		if ((m_ActiveEnemies < 3) && (m_ActiveEnemies < m_EnemiesLeft))
		{
			m_pEnemies.push_back(new Enemy(m_pScene, m_pDigger, m_StartPos, m_Dimensions, m_Grid));
			m_EnemyTimer = 0;
			m_ActiveEnemies++;
		}
	}

	for (Enemy* pEnemy : m_pEnemies)
	{
		if (pEnemy)
			pEnemy->Update();
	}
}

void Law::EnemyManager::StartLevel(int levelNr)
{
	for (size_t i{}; i < m_pEnemies.size(); i++)
	{
		m_pEnemies[i]->Disable();
		delete m_pEnemies[i];
		m_pEnemies[i] = nullptr;
	}
	m_pEnemies.clear();

	m_EnemiesLeft = levelNr + 5;
	m_ActiveEnemies = 0;
	m_EnemyTimer = 0.f;
}

void Law::EnemyManager::RestartLevel()
{
	for (size_t i{}; i < m_pEnemies.size(); i++)
	{
		m_pEnemies[i]->Disable();
		delete m_pEnemies[i];
		m_pEnemies[i] = nullptr;
	}
	m_pEnemies.clear();

	m_EnemyTimer = 0.f;
	m_ActiveEnemies = 0;
}

void Law::EnemyManager::RemoveEnemy()
{
	m_ActiveEnemies--;
}
