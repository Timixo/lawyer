#pragma once
#include <Scene.h>
#include <Helpers.h>
#include <map>
#include <vector>

namespace Law
{
	class Bag;
	class Digger;
	class Emerald;
	class Subject;
	class Background;
	class MoveCommand;
	class EnemyManager;
	class StartCommand;
	class LifeObserver;
	class TextComponent;
	class PointsObserver;
	class ContactListener;
	class TextureComponent;

	class Game final : public Scene
	{
	public:
		Game();
		virtual ~Game();

		virtual void Initialize() override;
		virtual void PhysicsUpdate() override;
		virtual void Update() override;
		virtual void Render() const override;

		void NextLevel();
		void CleanLevel();

		void GameOver();

	private:
		bool m_NeedsRestart;

		int m_CurrentLevel;

		static int m_TopBorder;
		static int m_LeftBorder;
		
		Point2i m_WindowDim;

		EnemyManager* m_pEnemyManager;

		Digger* m_pDigger;

		Grid m_Grid;

		Subject* m_pSubject;
		LifeObserver* m_pLifeObserver;
		PointsObserver* m_pPointsObserver;
		
		TextComponent* m_pPoints;
		TextureComponent* m_pStartScreen;
		std::vector<TextureComponent*> m_pLifeTextures;

		ContactListener* m_pContactListener;

		StartCommand* m_pStartCommand;

		std::vector<Bag*> m_pBags;
		std::vector<Emerald*> m_pEmeralds;
		std::vector<Background*> m_pBackgrounds;

		std::map<int, std::vector<std::vector<char>>> m_Levels;

		void MakeGrid();
		void GenerateLevel(int levelNr);
	};
}