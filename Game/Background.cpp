#include "Background.h"
#include <Scene.h>
#include <GameObject.h>
#include <Components.h>
#include <ResourceManager.h>

Law::Background::Background(Scene* scene, int level, Point2i pos, Point2i dim)
{
	for (int i{}; i < 4; i++)
	{
		for (int row{}; row < 2; row++)
		{
			for (int col{}; col < 4; col++)
			{
				int index{ (row * 4) + col };

				GameObject* go = new GameObject(ObjectType::BACKGROUND);
				go->GetTransform()->SetPosition(float(pos.x + (col * dim.x / 4.f)), pos.y + (row * dim.y / 8.f) + (i * dim.y / 4.f), 0.f);
				TextureComponent* texture = new TextureComponent();

				switch (index)
				{
				case 0:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/0.png", Texture(level * 8 + int(Texture::BACKGROUND10)), 1);
					break;
				case 1:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/1.png", Texture(level * 8 + int(Texture::BACKGROUND11)), 1);
					break;
				case 2:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/2.png", Texture(level * 8 + int(Texture::BACKGROUND12)), 1);
					break;
				case 3:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/3.png", Texture(level * 8 + int(Texture::BACKGROUND13)), 1);
					break;
				case 4:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/4.png", Texture(level * 8 + int(Texture::BACKGROUND14)), 1);
					break;
				case 5:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/5.png", Texture(level * 8 + int(Texture::BACKGROUND15)), 1);
					break;
				case 6:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/6.png", Texture(level * 8 + int(Texture::BACKGROUND16)), 1);
					break;
				case 7:
					texture->SetTexture("Sprites/Background/Level" + std::to_string(level) + "/7.png", Texture(level * 8 + int(Texture::BACKGROUND17)), 1);
					break;
				}

				texture->SetDimensions({ dim.x / 4 + 1, dim.y / 8 + 1 });
				texture->SetShow(true);
				go->AddComponent(texture);

				PhysicsComponent* pPhysics = new PhysicsComponent(b2Vec2{ float(pos.x + (col * dim.x / 4.f)), pos.y + (row * dim.y / 8.f) + (i * dim.y / 4.f) },
					b2Vec2{ dim.x / 4.f, dim.y / 8.f }, ObjectType::BACKGROUND
					, ObjectType::DIGGER | ObjectType::BAG | ObjectType::BAGBREAKER | ObjectType::BAGSENSOR | ObjectType::FIREBALL
				| ObjectType::ENEMY | ObjectType::ENEMYTOP | ObjectType::ENEMYRIGHT | ObjectType::ENEMYDOWN | ObjectType::ENEMYLEFT);

				go->AddComponent(pPhysics);

				scene->Add(go);
				m_pObjects.push_back(go);
			}
		}
	}
}

void Law::Background::Disable()
{
	for (size_t i{}; i < m_pObjects.size(); i++)
	{
		m_pObjects[i]->Disable();
	}
}
