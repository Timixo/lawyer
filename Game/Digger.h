#pragma once
#include "Entity.h"

namespace Law
{
	class PhysicsComponent;
	class SpriteComponent;
	class FireBall;
	class Subject;
	class Digger final : public Entity
	{
	public: 
		Digger(Scene* scene, Subject* pSubject, Point2i pos, Point2i dim, const Grid& grid);
		~Digger();

		virtual void PhysicsUpdate() override;
		virtual void Update() override;
		virtual void Dies() override;

		void Fire();
		void Restart();
		void HitsBag(bool hits);
		void MakeInvincible();

		bool CanMoveUp();
		bool IsInvincible();

	private:
		bool m_CanFire;
		bool m_NeedsRestart;
		bool m_CanMoveUp;
		bool m_IsInvincible;

		float m_FireTimer;
		float m_DiedTimer;
		float m_InvincibleTimer;
		const float m_FireRate;
		const float m_TimeDead;
		const float m_TimeInvincible;

		Point2i m_StartPos;
		Point2i m_Dimensions;

		Scene* m_pScene;

		FireBall* m_pFireball;

		SpriteComponent* m_pSprite;
		SpriteComponent* m_pDiedSprite;

		PhysicsComponent* m_pPhysics;

		Subject* m_pSubject;
	};
}