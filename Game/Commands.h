#pragma once
#include "Command.h"
#include "Helpers.h"
#include <Windows.h>
#include <XInput.h>

namespace Law
{
	class Entity;
	class FireBall;
	class TextureComponent;
	class PhysicsComponent;
	class TransformComponent;
	class Game;

	class StartCommand final : public Command
	{
	public:
		StartCommand();

		virtual void Execute(int playerIdx) override;
		virtual void AddPlayer(int playerIdx, Component* component) override;

		void Activate(bool activate);
		void AddGame(Game* pGame);
		void AddStartScreen(TextureComponent* pStartScreen);

	private:
		bool m_IsActivated;

		Game* m_pGame;
		TextureComponent* m_pStartScreen;
	};

	class ControllerCommand : public Command
	{
	public:
		ControllerCommand();

		virtual void Execute(int playerIdx) = 0;
		virtual void AddPlayer(int playerIdx, Component* component) override;

	protected:
		PhysicsComponent* m_pPlayerPhysics[XUSER_MAX_COUNT];
	};

	class MoveCommand final : public ControllerCommand
	{
	public:
		MoveCommand(MovementState movement, const Grid& grid);

		virtual void Execute(int playerIdx) override;
		void AddOwner(Entity* pOwner);

	private:
		Entity* m_Owner;

		Grid m_Grid;

		MovementState m_MoveState;

		TransformComponent* m_pTransform;
	};

	class FireCommand final : public ControllerCommand
	{
	public:
		virtual void Execute(int playerIdx) override;
		void AddOwner(Entity* pOwner);

	private:
		Entity* m_Owner;
	};
}