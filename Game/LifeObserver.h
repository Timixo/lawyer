#pragma once
#include <Observer.h>

namespace Law
{
	class Game;
	class LifeObserver final : public Observer
	{
	public:
		LifeObserver(Game* pGame);

		virtual void OnNotify(Event event) override;

		int GetLives();

	private:
		int m_Lives;
		const int m_MaxLives;

		Game* m_pGame;
	};
}