#include "LifeObserver.h"
#include "Game.h"

Law::LifeObserver::LifeObserver(Game* pGame)
	: m_MaxLives{3}
	, m_pGame{pGame}
{
	m_Lives = m_MaxLives;
}

void Law::LifeObserver::OnNotify(Event event)
{
	switch (event)
	{
	case Law::Event::ON_DIGGER_DIES:
		m_Lives--;
		if (m_Lives == 0)
		{
			m_pGame->GameOver();
			m_Lives = m_MaxLives;
		}
		break;
	case Law::Event::ON_DIGGER_GAIN_LIFE:
		m_Lives++;
	}
}

int Law::LifeObserver::GetLives()
{
	return m_Lives;
}
