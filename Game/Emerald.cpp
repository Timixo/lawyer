#include "Emerald.h"
#include "Background.h"
#include <Scene.h>
#include <Logger.h>
#include <GameObject.h>
#include <Components.h>
#include <ResourceManager.h>

Law::Emerald::Emerald(Scene* scene, Point2i pos, Point2i dim)
{
	m_pObject = new GameObject(ObjectType::EMERALD);

	m_pObject->SetUserData(this);

	TextureComponent* texture = new TextureComponent();
	texture->SetTexture("Sprites/Emerald.png", Texture::EMERALD, 1);
	Point2i textureDim{ texture->GetDimension() };
	texture->SetShow(true);
	m_pObject->GetTransform()->SetPosition(float(pos.x + (dim.x - textureDim.x) / 2), float(pos.y + (dim.y - textureDim.y) / 2), 0.5f);
	m_pObject->AddComponent(texture);

	PhysicsComponent* pPhysics = new PhysicsComponent(b2Vec2{ float(pos.x + (dim.x - textureDim.x) / 2 - 5.f), float(pos.y + (dim.y - textureDim.y) / 2) }, b2Vec2{ dim.x - 10.f, dim.y - 20.f }, ObjectType::EMERALD, ObjectType::DIGGER);
	m_pObject->AddComponent(pPhysics);

	scene->Add(m_pObject);
}

void Law::Emerald::Take()
{
	m_pObject->Disable();
}

void Law::Emerald::Disable()
{
	m_pObject->Disable();
}
