#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"
#include <algorithm>

void Law::SceneManager::Initialize()
{
	for (auto& scene : m_pScenes)
	{
		scene->Initialize();
	}
}

void Law::SceneManager::PhysicsUpdate()
{
	for (auto& scene : m_pScenes)
	{
		scene->RootPhysicsUpdate();
	}
}

void Law::SceneManager::Update()
{
	for(auto& scene : m_pScenes)
	{
		scene->RootUpdate();
	}
}

void Law::SceneManager::Render()
{
	for (const auto& scene : m_pScenes)
	{
		scene->RootRender();
	}
}

void Law::SceneManager::AddScene(Scene* pScene)
{
	if (std::find_if(m_pScenes.cbegin(), m_pScenes.cend(), [pScene](Scene* pOther) {return pScene == pOther; }) == m_pScenes.cend())
		m_pScenes.push_back(pScene);
}

Law::SceneManager::~SceneManager()
{
	for (size_t i{}; i < m_pScenes.size(); i++)
	{
		delete m_pScenes[i];
	}
	m_pScenes.clear();
}