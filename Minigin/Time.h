#pragma once
#include "Singleton.h"
#include "Minigin.h"

namespace Law
{
	class Time final
	{
	public:
		static Time* GetInstance();

		static void Update(float deltaTime);

		static int GetFPS();
		static float GetDeltaTime();
		static const float GetFixedTimeStep();

		static const float GetMaxTimePerFrame();

		static void SetTimeMultiplier(float multiplier);

	private:
		static int m_FPS;
		static int m_FramesPassed;
		static const int m_MaxFramesPerSec;

		static float m_DeltaTime;
		static float m_FPSTimer;
		static float m_FixedTimeStep;
		static float m_TimeMultiplier;
		
		static Time* m_pInstance;

		Time() = default;
		~Time() { delete m_pInstance; };
	};
}