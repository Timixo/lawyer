#include "MiniginPCH.h"
#include <SDL_ttf.h>
#include "Font.h"

TTF_Font* Law::Font::GetFont() const {
	return m_Font;
}

Law::Font::Font(const std::string& fullPath, unsigned int size) : m_Font(nullptr), m_Size(size)
{
	m_Font = TTF_OpenFont(fullPath.c_str(), size);
	if (m_Font == nullptr) 
	{
		Logger::LogError("Font::Font >> Failed to load font");
		throw std::runtime_error(std::string("Font::Font >> Failed to load font: ") + SDL_GetError());
	}
}

Law::Font::~Font()
{
	TTF_CloseFont(m_Font);
}
