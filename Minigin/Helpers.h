#pragma once
#include <vector>
#include <unordered_map>

namespace Law
{
	// ENUMS
	enum class GamepadIndex
	{
		P1 = 0,
		P2 = 1,
		P3 = 2,
		P4 = 3
	};

	enum class InputTriggerState
	{
		PRESSED = 0,
		RELEASED = 1,
		DOWN = 2
	};

	enum class ControllerButton
	{
		BUTTONA,
		BUTTONB,
		BUTTONX,
		BUTTONY
	};

	enum class Texture
	{
		BACKGROUND10 = 0,
		BACKGROUND11 = 1,
		BACKGROUND12 = 2,
		BACKGROUND13 = 3,
		BACKGROUND14 = 4,
		BACKGROUND15 = 5,
		BACKGROUND16 = 6,
		BACKGROUND17 = 7,
		BACKGROUND20 = 8,
		BACKGROUND21 = 9,
		BACKGROUND22 = 10,
		BACKGROUND23 = 11,
		BACKGROUND24 = 12,
		BACKGROUND25 = 13,
		BACKGROUND26 = 14,
		BACKGROUND27 = 15,
		BACKGROUND30 = 16,
		BACKGROUND31 = 17,
		BACKGROUND32 = 18,
		BACKGROUND33 = 19,
		BACKGROUND34 = 20,
		BACKGROUND35 = 21,
		BACKGROUND36 = 22,
		BACKGROUND37 = 23,
		BACKGROUND40 = 24,
		BACKGROUND41 = 25,
		BACKGROUND42 = 26,
		BACKGROUND43 = 27,
		BACKGROUND44 = 28,
		BACKGROUND45 = 29,
		BACKGROUND46 = 30,
		BACKGROUND47 = 31,
		BACKGROUND50 = 32,
		BACKGROUND51 = 33,
		BACKGROUND52 = 34,
		BACKGROUND53 = 35,
		BACKGROUND54 = 36,
		BACKGROUND55 = 37,
		BACKGROUND56 = 38,
		BACKGROUND57 = 39,
		BACKGROUND60 = 40,
		BACKGROUND61 = 41,
		BACKGROUND62 = 42,
		BACKGROUND63 = 43,
		BACKGROUND64 = 44,
		BACKGROUND65 = 45,
		BACKGROUND66 = 46,
		BACKGROUND67 = 47,
		BACKGROUND70 = 48,
		BACKGROUND71 = 49,
		BACKGROUND72 = 50,
		BACKGROUND73 = 51,
		BACKGROUND74 = 52,
		BACKGROUND75 = 53,
		BACKGROUND76 = 54,
		BACKGROUND77 = 55,
		BACKGROUND80 = 56,
		BACKGROUND81 = 57,
		BACKGROUND82 = 58,
		BACKGROUND83 = 59,
		BACKGROUND84 = 60,
		BACKGROUND85 = 61,
		BACKGROUND86 = 62,
		BACKGROUND87 = 63,
		PLAYER = 64,
		EMERALD = 65,
		BAG = 66,
		COINS = 67,
		FIRE = 68,
		EXPLOSION = 69,
		GRAVE = 70,
		NOBBIN = 71,
		HOBBIN = 72,
		TITLE = 73
	};

	enum class Channel
	{
		BACKGROUND = 0,
		NRCHANNELS = 1
	};

	enum class Sound
	{
		BACKGROUND = 0,
		NRSOUNDS = 1
	};

	enum class State
	{
		NONE = 0,
		MOVING = 1,
		FALLING = 2
	};

	enum class MovementState
	{
		NONE = 0,
		DOWN = 1,
		LEFT = 2,
		RIGHT = 3,
		UP = 4
	};

	enum class SpriteState
	{
		NONE = 0,
		MOVINGDOWN = 1,
		MOVINGLEFT = 2,
		MOVINGRIGHT = 3,
		MOVINGUP = 4,
		BTOPLAYER = 5,
		BDOWNLAYER = 6,
		FIRING = 7
	};

	enum ObjectType
	{
		NONE = 0x0000,
		DIGGER = 0x0001,
		BACKGROUND = 0x0002,
		BAG = 0x0004,
		BAGSENSOR = 0x0008,
		BAGBREAKER = 0x0010,
		BAGLEFT = 0x0020,
		BAGRIGHT = 0x0040,
		EMERALD = 0x0080,
		WALL = 0x0100,
		FIREBALL = 0x0200,
		ENEMY = 0x0400,
		ENEMYTOP = 0x0800,
		ENEMYRIGHT = 0x1000,
		ENEMYDOWN = 0x2000,
		ENEMYLEFT = 0x4000
	};

	enum class Event
	{
		ON_TAKE_EMERALD = 0,
		ON_TAKE_GOLD = 1,
		ON_DIGGER_DIES = 2,
		ON_ENEMY_DIES = 3,
		ON_DIGGER_GAIN_LIFE = 4,
		ON_RESTART = 5
	};

	// STRUCTS
	struct Point2i
	{
		int x;
		int y;

		float Length()
		{
			return sqrtf(float(pow(x, 2) + pow(y, 2)));
		}

		float SquaredLength()
		{
			return float(pow(x, 2) + pow(y, 2));
		}

		Point2i operator-(const Point2i& rhs)
		{
			Point2i result{ x - rhs.x, y - rhs.y };
			return result;
		}

		bool operator==(const Point2i& rhs)
		{
			return ((x == rhs.x) && (y == rhs.y));
		}
	};

	struct Point2f
	{
		float x;
		float y;

		float Length()
		{
			return sqrtf(pow(x, 2) + pow(y, 2));
		}

		float SquaredLength()
		{
			return (pow(x, 2) + pow(y, 2));
		}

		Point2f operator-(const Point2f& rhs)
		{
			Point2f result{ x - rhs.x, y - rhs.y };
			return result;
		}

		bool operator==(const Point2f& rhs)
		{
			return ((abs(x - rhs.x) < FLT_EPSILON) && (abs(y - rhs.y) < FLT_EPSILON));
		}
	};

	struct SpriteSheetInfo
	{
		int rows;
		int columns;

		int width;
		int height;

		std::unordered_map<SpriteState, std::pair<int, int>> spriteSheet;
	};

	struct Node
	{
		Point2f data;

		Node* pTop = nullptr;
		Node* pRight = nullptr;
		Node* pDown = nullptr;
		Node* pLeft = nullptr;
	};

	struct Grid
	{
		int rows;
		int columns;
		int cellWidth;
		int cellHeight;
		
		Point2i gamePos;
		Point2i gameDim;

		std::vector<std::vector<Node*>> grid;

		int GetIdxFromCoord(Point2i coord)
		{
			int row = ((gamePos.y + gameDim.y) - coord.y - 1) / cellHeight;
			int column = (coord.x - gamePos.x) / cellWidth;
			return ((row * columns) + column);
		}

		int GetIdxFromCoord(Point2f coord) 
		{
			int row = ((gamePos.y + gameDim.y) - (int)coord.y - 1) / cellHeight; 
			int column = ((int)coord.x - gamePos.x) / cellWidth;
			return ((row * columns) + column);
		}

		Point2f GetCoordFromIdx(int idx)
		{
			int row{ idx / columns };
			int column{ idx % columns };

			return grid[row][column]->data;
		}
	};
}