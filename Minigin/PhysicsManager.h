#pragma once
#include "Singleton.h"

class b2World;
class b2Draw;

namespace Law
{
	class PhysicsManager final : public Singleton<PhysicsManager>
	{
	public:
		~PhysicsManager();

		void Initialize();
		void Update(float deltaTime);

		b2World* GetWorld();

	private:
		friend class Singleton<PhysicsManager>;
		PhysicsManager() = default;

		static const int m_VelocityIt;
		static const int m_PositionIt;
		static const float m_Gravity;

		b2World* m_pWorld;
	};
}