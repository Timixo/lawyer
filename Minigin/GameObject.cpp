#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "TransformComponent.h"
#include <algorithm>

Law::GameObject::GameObject(ObjectType type)
	: m_State{State::NONE}
	, m_MovementState{MovementState::NONE}
	, m_ObjectType{type}
	, m_pUserData{nullptr}
{
	AddComponent(new TransformComponent());
}

Law::GameObject::~GameObject()
{
	for (size_t i{}; i < m_pComponents.size(); i++)
	{
		if (m_pComponents[i])
			delete m_pComponents[i];
	}
	m_pComponents.clear();
}

void Law::GameObject::AddComponent(Component* pComponent)
{
	m_pComponents.push_back(pComponent);
	pComponent->SetOwner(this);
	
	if (!pComponent)
		Logger::LogInfo("GameObject::AddComponent >> component added was nullptr");
}

bool Law::GameObject::GetDisabled()
{
	return m_IsDisabled;
}

Law::ObjectType Law::GameObject::GetObjectType()
{
	return m_ObjectType;
}

void* Law::GameObject::GetUserData()
{
	return m_pUserData;
}

void Law::GameObject::SetUserData(void* pUserData)
{
	m_pUserData = pUserData;
}

void Law::GameObject::Disable()
{
	m_IsDisabled = true;
}

void Law::GameObject::RootPhysicsUpdate()
{
	if (m_IsDisabled)
		return;

	PhysicsUpdate();
	for (Component* pComp : m_pComponents)
		pComp->PhysicsUpdate();
}

void Law::GameObject::RootUpdate()
{
	if (m_IsDisabled)
		return;

	Update();
	for (Component* pComp : m_pComponents)
		pComp->Update();
}

void Law::GameObject::RootRender() const
{
	if (m_IsDisabled)
		return;

	Render();
	for (Component* pComp : m_pComponents)
		pComp->Render();
}

Law::TransformComponent* Law::GameObject::GetTransform()
{
	return GetComponent<TransformComponent>();
}
