#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"
#include <algorithm>

using namespace Law;

unsigned int Scene::m_IdCounter = 0;

Scene::Scene(const std::string& name) : m_Name(name) {}

Scene::~Scene()
{
	for (size_t i{}; i < m_pObjects.size(); i++)
	{
		if (m_pObjects[i])
			delete m_pObjects[i];
	}
	m_pObjects.clear();
}

void Scene::Add(GameObject* object)
{
	m_pObjects.push_back(object);
}

void Law::Scene::RootPhysicsUpdate()
{
	PhysicsUpdate();
	for (auto& object : m_pObjects)
	{
		object->PhysicsUpdate();
	}
}

void Scene::RootUpdate()
{
	Update();
	for(auto& object : m_pObjects)
	{
		object->RootUpdate();
	}
	
	m_pObjects.erase(std::remove_if(m_pObjects.begin(), m_pObjects.end(), [](GameObject* pObject)
		{
			if (pObject->GetDisabled())
			{
				delete pObject;
				return true;
			}
			return false;
		}), m_pObjects.end());
}

void Scene::RootRender() const
{
	Render();
	for (const auto& object : m_pObjects)
	{
		object->RootRender();
	}
}

