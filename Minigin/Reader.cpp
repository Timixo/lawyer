#include "MiniginPCH.h"
#include "Reader.h"
#include "ResourceManager.h"
#include <fstream>

void Law::Reader::Open(const std::string& file)
{
	std::string path{ ResourceManager::GetInstance().GetDataPath() };
	path += file;

	m_pReader = new std::ifstream();
	m_pReader->open(path);
	if (!m_pReader)
	{
		Logger::LogWarning("File given to open is not available");
		return;
	}
	m_IsOpen = true;
}

void Law::Reader::Close()
{
	if (m_pReader)
	{
		m_pReader->close();
		delete m_pReader;
	}
	m_IsOpen = false;
}

std::string Law::Reader::ReadLine()
{
	std::string line{};

	if (m_pReader->eof())
	{
		line = "EOF";
		return line;
	}

	std::getline(*m_pReader, line);
	return line;
}
