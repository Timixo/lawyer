#pragma once
#include "Singleton.h"
#include "Helpers.h"
#include <map>

namespace Law
{
	class Texture2D;
	class Font;
	class ResourceManager final : public Singleton<ResourceManager>
	{
	public:
		virtual ~ResourceManager();

		void Init(const std::string& data);
		Texture2D* LoadTexture(const std::string& file, Texture texture);
		Font* LoadFont(const std::string& file, unsigned int size) const;

		const std::string& GetDataPath() const;

	private:
		friend class Singleton<ResourceManager>;
		ResourceManager() = default;
		std::string m_DataPath;

		std::map<Texture, Texture2D*> m_pTextures;
	};
}
