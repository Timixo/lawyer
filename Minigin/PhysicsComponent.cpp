#include "MiniginPCH.h"
#include "PhysicsComponent.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "PhysicsManager.h"
#include "Renderer.h"

float Law::PhysicsComponent::m_PPM{ 32.f };

Law::PhysicsComponent::PhysicsComponent(b2Vec2 position, b2Vec2 dimensions, uint16 categoryBits, uint16 maskBits, BodyShape shape, bool isStatic, bool isSubjectGravity, bool isSensor, float density, float friction)
	: m_IsStatic{isStatic}
	, m_IsSubjectGravity{isSubjectGravity}
	, m_Position{position}
	, m_Dimensions{dimensions}
	, m_pBody{nullptr}
{
	if (isStatic)
	{
		b2BodyDef staticBodyDef{};
		staticBodyDef.position.Set((position.x + (dimensions.x / 2)) / m_PPM, (position.y + (dimensions.y / 2)) / m_PPM);

		m_pBody = Law::PhysicsManager::GetInstance().GetWorld()->CreateBody(&staticBodyDef);
	}
	else
	{
		b2BodyDef dynamicBodyDef{};
		dynamicBodyDef.type = b2_dynamicBody;
		dynamicBodyDef.position.Set((position.x + (dimensions.x / 2)) / m_PPM, (position.y + (dimensions.y / 2)) / m_PPM);
		m_pBody = Law::PhysicsManager::GetInstance().GetWorld()->CreateBody(&dynamicBodyDef);
	}

	if (!m_pBody)
	{
		Logger::LogError("PhysicsComponent::PhysicsComponent >> BodyCreation failed");
		throw std::runtime_error("PhysicsComponent::PhysicsComponent >> BodyCreation failed");
	}

	b2FixtureDef fixtureDef;
	b2PolygonShape rect{};
	b2CircleShape circle{};

	switch (shape)
	{
	case Law::BodyShape::RECTANGLE:
		rect.SetAsBox((dimensions.x / m_PPM) / 2, (dimensions.y / m_PPM) / 2);
		fixtureDef.shape = &rect;
		break;
	case Law::BodyShape::CIRCLE:
		circle.m_p.Set(0.f, 0.f);
		circle.m_radius = (dimensions.x < dimensions.y ? ((dimensions.x / 2) / m_PPM) : ((dimensions.y / 2) / m_PPM));
		fixtureDef.shape = &circle;
		break;
	}

	fixtureDef.filter.categoryBits = categoryBits;
	fixtureDef.filter.maskBits = maskBits;
	fixtureDef.density = density;
	fixtureDef.friction = friction;
	fixtureDef.isSensor = isSensor;
	fixtureDef.userData = this;

	m_pBody->CreateFixture(&fixtureDef);

	if (!isSubjectGravity)
		m_pBody->SetGravityScale(0);

	m_pBody->SetUserData(this);
	m_pBody->SetAwake(true);
}

Law::PhysicsComponent::~PhysicsComponent()
{
	if (GetGameObject()->GetDisabled())
		PhysicsManager::GetInstance().GetWorld()->DestroyBody(m_pBody);
}

void Law::PhysicsComponent::Update()
{
	b2Vec2 linVel{ m_pBody->GetLinearVelocity() };

	if (!m_HasOwnLinVel)
		m_pBody->SetLinearVelocity(b2Vec2{ 0, (m_IsSubjectGravity ? linVel.y : 0) });

	b2Vec2 pos{ m_pBody->GetPosition() };
	m_Position.x = pos.x * m_PPM;
	m_Position.y = pos.y * m_PPM;

	GetOwner()->GetTransform()->SetPosition(m_Position.x - (m_Dimensions.x / 2), m_Position.y - (m_Dimensions.y / 2), 0);
}

void Law::PhysicsComponent::Render() const
{
	#if defined(DEBUG) | defined(_DEBUG)
	//for (b2Fixture* pFixture{ m_pBody->GetFixtureList() }; pFixture; pFixture = pFixture->GetNext())
	//{
	//	b2Shape::Type type = pFixture->GetType();
	//	b2Vec2 bodyPos = m_pBody->GetPosition();

	//	switch (type)
	//	{
	//	case b2Shape::e_circle:
	//	{
	//		b2CircleShape* pCircle = static_cast<b2CircleShape*>(pFixture->GetShape());
	//		Renderer::GetInstance().DrawCircle(int((bodyPos.x + pCircle->m_p.x) * m_PPM), int((bodyPos.y + pCircle->m_p.y) * m_PPM), int(pCircle->m_radius * m_PPM));
	//	}
	//		break;
	//	case b2Shape::e_polygon:
	//	{
	//		b2PolygonShape* pRect = static_cast<b2PolygonShape*>(pFixture->GetShape());
	//		int xPos{ int((bodyPos.x + pRect->m_vertices[0].x) * m_PPM) };
	//		int yPos{ int((bodyPos.y + pRect->m_vertices[0].y) * m_PPM) };
	//		int width{ int((pRect->m_vertices[2].x - pRect->m_vertices[0].x) * m_PPM) };
	//		int height{ int((pRect->m_vertices[2].y - pRect->m_vertices[0].y) * m_PPM) };
	//		Renderer::GetInstance().DrawRect(xPos, yPos, width, height);
	//	}
	//		break;
	//	default:
	//		break;
	//	}
	//}
	#endif
}

b2Body* Law::PhysicsComponent::GetBody() const
{
	return m_pBody;
}

float Law::PhysicsComponent::GetPPM()
{
	return m_PPM;
}

void Law::PhysicsComponent::SetPPM(float ppm)
{
	m_PPM = ppm;
}

void Law::PhysicsComponent::SetStatic()
{
	m_IsStatic = true;
	m_pBody->SetType(b2_staticBody);
}

void Law::PhysicsComponent::SetDynamic()
{
	m_IsStatic = false;
	m_pBody->SetType(b2_dynamicBody);
}

void Law::PhysicsComponent::SetKinematic()
{
	m_IsStatic = false;
	m_pBody->SetType(b2_kinematicBody);
}

void Law::PhysicsComponent::SetPosition(Point2f pos)
{
	b2Vec2 vecPos{ pos.x, pos.y };
	vecPos.x /= m_PPM;
	vecPos.y /= m_PPM;
	m_pBody->SetTransform(vecPos, m_pBody->GetAngle());
}

void Law::PhysicsComponent::SetSubjectGravity(bool subject)
{
	m_IsSubjectGravity = subject;
}

void Law::PhysicsComponent::SetLinearVelocity(b2Vec2 linVel)
{
	m_pBody->SetLinearVelocity(linVel);
	m_HasOwnLinVel = true;
}

void Law::PhysicsComponent::AddFixture(b2Vec2 position, b2Vec2 dimensions, uint16 categoryBits, uint16 maskBits, BodyShape shape, bool isSensor)
{
	b2FixtureDef fixtureDef;
	b2PolygonShape rect{};
	b2CircleShape circle{};

	switch (shape)
	{
	case Law::BodyShape::RECTANGLE:
		rect.SetAsBox((dimensions.x / m_PPM) / 2, (dimensions.y / m_PPM) / 2, b2Vec2{position.x / m_PPM, position.y / m_PPM}, 0.f);
		fixtureDef.shape = &rect;
		break;
	case Law::BodyShape::CIRCLE:
		circle.m_p.Set(position.x / m_PPM, position.y / m_PPM);
		circle.m_radius = (dimensions.x < dimensions.y ? ((dimensions.x / 2) / m_PPM) : ((dimensions.y / 2) / m_PPM));
		fixtureDef.shape = &circle;
		break;
	}

	fixtureDef.filter.categoryBits = categoryBits;
	fixtureDef.filter.maskBits = maskBits;
	fixtureDef.density = 0.f;
	fixtureDef.friction = 0.f;
	fixtureDef.isSensor = isSensor;
	fixtureDef.userData = this;

	m_pBody->CreateFixture(&fixtureDef);

	m_pBody->SetUserData(this);
}
