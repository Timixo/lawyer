#pragma once
#include "Helpers.h"
#include <exception>

namespace Law
{
	class Texture2D;
	class TransformComponent;
	class Component;
	class GameObject final
	{
	public:
		void RootPhysicsUpdate();
		void RootUpdate();
		void RootRender() const;

		virtual void PhysicsUpdate() {};
		virtual void Update() {};
		virtual void Render() const {};

		TransformComponent* GetTransform();

		GameObject(ObjectType type);
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

		void Disable();
		void AddComponent(Component* pComponent);
		void SetUserData(void* pUserData);

		bool GetDisabled();
		ObjectType GetObjectType();
		void* GetUserData();

		template <typename T>
		T* GetComponent() const;

	private:
		bool m_IsDisabled;

		State m_State;
		MovementState m_MovementState;

		ObjectType m_ObjectType;

		void* m_pUserData;

		std::vector<Component*> m_pComponents;
	};

	template<class T>
	inline T* GameObject::GetComponent() const
	{
		for (Component* pComponent : m_pComponents)
		{
			T* tempComp = dynamic_cast<T*>(pComponent);
			if (tempComp) 
				return tempComp;
		}
		return nullptr;
		//throw std::exception("This component is not available in this gameobject");
	}
}