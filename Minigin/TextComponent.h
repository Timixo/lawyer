#pragma once
#include "Component.h"
#include <string>

namespace Law
{
	class Font;
	class Texture2D;
	class TextComponent final : public Component
	{
	public:
		void Update() override;
		void Render() const override;

		void SetText(const std::string& text);
		void SetFont(const std::string& fontPath, unsigned int size);

		TextComponent() = default;
		virtual ~TextComponent();
		TextComponent(const TextComponent& other) = delete;
		TextComponent(TextComponent&& other) = delete;
		TextComponent& operator=(const TextComponent& other) = delete;
		TextComponent& operator=(TextComponent&& other) = delete;

	private:
		bool m_NeedsUpdate = false;
		std::string m_Text;
		Font* m_pFont = nullptr;
		Texture2D* m_pTexture = nullptr;
	};
}
