#pragma once
#include "Component.h"
#include "Helpers.h"

struct SDL_Rect;

namespace Law
{
	class Texture2D;

	class TextureComponent : public Component
	{
	public:
		TextureComponent() = default;
		~TextureComponent();

		virtual void Update() override;
		virtual void Render() const override;

		void SetTexture(const std::string& fileName, Texture texture, float multiplier);
		void SetSource(Point2f pos, Point2f dim);
		void SetDimensions(Point2i dim);
		void SetShow(bool show);

		Point2i GetDimension() const;

	protected:
		bool m_HasCustomDimensions;
		bool m_IsShowing;

		float m_Multiplier;

		Point2i m_Dimension;

		Texture2D* m_pTexture = nullptr;
		SDL_Rect* m_pSourceRect = nullptr;
	};
}