#include "MiniginPCH.h"
#include "SpriteComponent.h"
#include "GameObject.h"
#include "Components.h"
#include "Texture2D.h"
#include "Renderer.h"
#include "Time.h"
#include <SDL.h>

float Law::SpriteComponent::m_StandardFrameTime{ 1.f / 16.f };

Law::SpriteComponent::SpriteComponent()
	: m_IsPaused{false}
	, m_IsLooping{true}
	, m_StopAfterLoop{false}
	, m_CurrentRow{0}
	, m_CurrentColumn{0}
	, m_PassedTime{0}
	, m_FrameTime{m_StandardFrameTime}
	, m_SpriteDim{}
	, m_CurrentState{}
	, m_Info{}
{
}

void Law::SpriteComponent::Update()
{
	if (m_IsPaused)
		return;

	m_PassedTime += Time::GetDeltaTime();

	if (m_PassedTime >= m_FrameTime)
	{
		m_PassedTime -= m_FrameTime;
		m_CurrentColumn++;
		m_CurrentColumn %= m_Info.spriteSheet[m_CurrentState].second;

		if ((m_CurrentColumn == 0) && !m_IsLooping)
		{
			m_CurrentColumn = m_Info.spriteSheet[m_CurrentState].second - 1;
			Pause();
			SetShow(!m_StopAfterLoop);
		}

		m_pSourceRect->x = m_CurrentColumn * m_pSourceRect->w;
	}
}

void Law::SpriteComponent::Render() const
{
	if (m_pTexture != nullptr)
	{
		if (m_IsShowing)
		{
			const auto pos{ GetGameObject()->GetTransform()->GetPosition() };

			Renderer::GetInstance().RenderTexture(*m_pTexture, pos.x, pos.y, float(m_Multiplier * m_SpriteDim.x), float(m_Multiplier * m_SpriteDim.y),
				m_pSourceRect);
		}
	}
	else
		Logger::LogWarning("TextureComponent::Render >> Trying to render texture without texture");
}

void Law::SpriteComponent::Play()
{
	m_CurrentColumn = 0;
	m_pSourceRect->x = 0;
	m_IsPaused = false;
	SetShow(true);
}

void Law::SpriteComponent::Pause()
{
	m_IsPaused = true;
}

void Law::SpriteComponent::SetSpriteSheet(const std::string& sheet, Texture texture, const SpriteSheetInfo& info, float multiplier)
{
	SetTexture(sheet, texture, multiplier);
	m_Info = info;
	m_Multiplier = multiplier;
	m_Info.width = m_Dimension.x;
	m_Info.height = m_Dimension.y;
	m_SpriteDim.x = m_Info.width / m_Info.columns;
	m_SpriteDim.y = m_Info.height / m_Info.rows;
	SetSource({ 0, 0 }, { float(m_Info.width / m_Info.columns), float(m_Info.height / m_Info.rows) });
}

void Law::SpriteComponent::SetSpriteState(SpriteState state)
{
	if (m_Info.spriteSheet.find(state) == m_Info.spriteSheet.end())
	{
		Logger::LogWarning("Tried to change sprite to unknown state for this sprite");
		return;
	}

	if (m_CurrentState == state)
		return;

	m_CurrentState = state;
	m_CurrentRow = m_Info.spriteSheet[state].first;
	m_CurrentColumn = 0;
	m_pSourceRect->x = 0;
	m_pSourceRect->y = m_CurrentRow * m_pSourceRect->h;
}

void Law::SpriteComponent::SetSpriteCol(int col)
{
	m_CurrentColumn = col % m_Info.columns;
	m_pSourceRect->x = m_CurrentColumn * m_SpriteDim.x;
}

void Law::SpriteComponent::SetFrameTime(float frameTime)
{
	m_FrameTime = frameTime;
}

void Law::SpriteComponent::SetLooping(bool looping, bool stopAfterLoop)
{
	m_IsLooping = looping;
	m_StopAfterLoop = stopAfterLoop;
}

bool Law::SpriteComponent::IsPaused()
{
	return m_IsPaused;
}
