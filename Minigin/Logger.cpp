#include "MiniginPCH.h"
#include "Logger.h"

void Law::Logger::LogInfo(const std::string& message)
{
	Log(LogLevel::Info, message);
}

void Law::Logger::LogWarning(const std::string& message)
{
	Log(LogLevel::Warning, message);
}

void Law::Logger::LogError(const std::string& message)
{
	Log(LogLevel::Error, message);
}

void Law::Logger::Log(LogLevel level, const std::string& message)
{
	std::stringstream stream{};

	switch (level)
	{
	case Law::Logger::LogLevel::Info:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		stream << "[INFO]: ";
		break;
	case Law::Logger::LogLevel::Warning:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN);
		stream << "[WARNING]: ";
		break;
	case Law::Logger::LogLevel::Error:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);
		stream << "[ERROR]: ";
		break;
	}

	stream << message;
	std::cout << stream.str() << std::endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}
