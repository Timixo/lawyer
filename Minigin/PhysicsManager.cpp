#include "MiniginPCH.h"
#include "PhysicsManager.h"
#include <Box2D.h>

const int Law::PhysicsManager::m_VelocityIt{ 8 };
const int Law::PhysicsManager::m_PositionIt{ 3 };
const float Law::PhysicsManager::m_Gravity{ 9.81f };

Law::PhysicsManager::~PhysicsManager()
{
	delete m_pWorld;
}

void Law::PhysicsManager::Initialize()
{
	m_pWorld = new b2World(b2Vec2{ 0, m_Gravity });
}

void Law::PhysicsManager::Update(float deltaTime)
{
	if (!m_pWorld)
	{
		Logger::LogError("PhysicsManager::Update >> Uninitialized Box2D world");
		throw std::runtime_error("PhysicsManager::Update >> Uninitialized Box2D world");
	}

	m_pWorld->Step(deltaTime, m_VelocityIt, m_PositionIt);
}

b2World* Law::PhysicsManager::GetWorld()
{
	return m_pWorld;
}
