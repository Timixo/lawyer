#include "MiniginPCH.h"
#include "Observer.h"
#include "Subject.h"

Law::Observer::Observer()
	: m_pNext{ nullptr }
	, m_pSubject{nullptr}
{
}

Law::Observer::~Observer()
{
	if (m_pSubject)
		m_pSubject->DeleteObserver(this);
}
