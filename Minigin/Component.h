#pragma once
namespace Law
{
	class GameObject;

	class Component
	{
	public:
		Component() = default;
		virtual ~Component() = default;

		virtual void PhysicsUpdate() {};
		virtual void Update() = 0;
		virtual void Render() const = 0;

		inline GameObject* GetGameObject() const { return m_Owner; }
		void SetOwner(GameObject* gameObject) { m_Owner = gameObject; }

	protected:
		GameObject* GetOwner() { return m_Owner; }

	private:
		GameObject* m_Owner = nullptr;
	};
}