#pragma once
#include "Singleton.h"
#include "Helpers.h"
#include <fmod.hpp>
#include <vector>

namespace Law
{
	class AudioManager final : public Singleton<AudioManager>
	{
	public:
		~AudioManager();

		void Initialize();

		bool AddChannel(Channel channel);
		bool AddSound(std::string file, Sound sound, FMOD_MODE mode);

		void SetVolume(float volume, Channel channel);

		void StartSound(Sound sound, Channel channel);
		void PauseChannel(bool pause, Channel channel);

	private:
		int m_NrChannels;

		FMOD::System* m_pSystem;

		std::vector<FMOD::Channel*> m_pChannels;
		std::vector<FMOD::Sound*> m_pSounds;
	};
}