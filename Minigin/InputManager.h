#pragma once
#include "Singleton.h"
#include "Helpers.h"
#include <XInput.h>
#include <map>
#include <vector>
#include <future>

namespace Law
{
	class Command;

	struct InputStruct
	{
		int Id;
		InputTriggerState TriggerState;
		// Work with VK_...
		int KeyboardCode;
		WORD GamepadButtonCode;
		int PlayerIndex;
		GamepadIndex PlayerGamepadIndex;
		Command* pCommand;

		InputStruct(int id, InputTriggerState triggerState, int playerIndex, Command* pcommand, int keyboardCode = -1, WORD gamepadButtonCode = 0)
			: Id{ id }
			, TriggerState{ triggerState }
			, KeyboardCode{ keyboardCode }
			, GamepadButtonCode{ gamepadButtonCode }
			, PlayerIndex{ playerIndex }
			, PlayerGamepadIndex{ (GamepadIndex)playerIndex }
			, pCommand{ pcommand }
		{}
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		virtual ~InputManager();

		bool ProcessInput();

		bool AddInput(const InputStruct& input);
		bool IsMouseButtonDown(int button, bool previousframe = false);
		bool IsKeyboardButtonDown(int key, bool previousframe = false);
		bool IsGamepadButtonDown(WORD button, GamepadIndex gamepadIndex, bool previousframe = false);

	private:
		// CONTROLLERS
		XINPUT_STATE m_OldGamepadState[XUSER_MAX_COUNT];
		XINPUT_STATE m_CurrentGamepadState[XUSER_MAX_COUNT];

		// KEYBOARD
		short m_OldKeyboardState[256];
		short m_CurrentKeyboardState[256];

		// MOUSE
		POINT m_MouseMovement;
		POINT m_OldMousePosition;
		POINT m_CurrentMousePosition;

		std::map<int, InputStruct> m_Inputs;
		std::vector<std::future<void>> m_InputThreads;

		friend class Singleton<InputManager>;
		InputManager();
	};
}
