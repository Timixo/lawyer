#pragma once
#include "Helpers.h"
struct SDL_Window;
namespace Law
{
	class Minigin
	{
	public:
		void Initialize();
		void Cleanup();
		void Run();

		static SDL_Window* GetWindow();
		static Point2i GetWindowDim();

	private:
		static Point2i m_WindowDim;
		static SDL_Window* m_Window;
	};
}