#include "MiniginPCH.h"
#include "AudioManager.h"
#include "ResourceManager.h"

Law::AudioManager::~AudioManager()
{
	for (FMOD::Sound* pSound : m_pSounds)
		pSound->release();

	m_pSystem->release();
}

void Law::AudioManager::Initialize()
{
	FMOD_RESULT result = FMOD::System_Create(&m_pSystem);
	if (result != FMOD_OK)
	{
		Logger::LogError("FMOD system not initialized");
	}

	unsigned int version{};

	result = m_pSystem->getVersion(&version);
	if (result != FMOD_OK)
	{
		Logger::LogError("FMOD system could not get version");
	}

	if (version < FMOD_VERSION)
	{
		Logger::LogError("SoundManager Initialization Failed!\n\nYou are using an old version of FMOD %08x. This program requires %08x\n");
		return;
	}

	int numdrivers{};

	result = m_pSystem->getNumDrivers(&numdrivers);
	if (result != FMOD_OK)
	{
		Logger::LogError("FMOD system could not get drivers");
	}

	if (numdrivers == 0)
	{
		result = m_pSystem->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
	}
	else
	{
		result = m_pSystem->init(32, FMOD_INIT_NORMAL, nullptr);
	}

	m_pChannels.resize((int)Channel::NRCHANNELS);
	m_pSounds.resize((int)Sound::NRSOUNDS);
}

bool Law::AudioManager::AddChannel(Channel channel)
{
	if (m_pChannels[(int)channel])
	{
		Logger::LogWarning("Channel was already added");
		return false;
	}

	FMOD::Channel* pChannel{};
	FMOD_RESULT result = m_pSystem->getChannel(m_NrChannels++, &pChannel);
	if (result != FMOD_OK)
	{
		Logger::LogWarning("Couldn't get channel");
		return false;
	}

	m_pChannels[(int)channel] = pChannel;
	return true;
}

bool Law::AudioManager::AddSound(std::string file, Sound sound, FMOD_MODE mode)
{
	if (m_pSounds[(int)sound])
	{
		Logger::LogWarning("Sound was already added");
		return false;
	}

	FMOD::Sound* pSound{};
	FMOD_RESULT result = m_pSystem->createSound((ResourceManager::GetInstance().GetDataPath() + file).c_str(), mode, nullptr, &pSound);
	if (result != FMOD_OK)
	{
		Logger::LogWarning("FMOD sound not initialized");
		return false;
	}

	m_pSounds[(int)sound] = pSound;
	return true;
}

void Law::AudioManager::SetVolume(float volume, Channel channel)
{
	m_pChannels[(int)channel]->setVolume(volume);
}

void Law::AudioManager::StartSound(Sound sound, Channel channel)
{
	m_pSystem->playSound(m_pSounds[(int)sound], nullptr, false, &m_pChannels[(int)channel]);
}

void Law::AudioManager::PauseChannel(bool pause, Channel channel)
{
	m_pChannels[(int)channel]->setPaused(pause);
}
