#include "MiniginPCH.h"
#include "Time.h"
#include "Time.h"

int Law::Time::m_FPS{ 0 };
int Law::Time::m_FramesPassed{ 0 };
const int Law::Time::m_MaxFramesPerSec{ 60 };

float Law::Time::m_DeltaTime{ 0.f };
float Law::Time::m_FPSTimer{ 0.f };
float Law::Time::m_FixedTimeStep{ 0.f };
float Law::Time::m_TimeMultiplier{ 1.0f };

Law::Time* Law::Time::m_pInstance{ nullptr };

Law::Time* Law::Time::GetInstance()
{
	if (!m_pInstance)
	{
		m_pInstance = new Time();
	}

	return m_pInstance;
}

void Law::Time::Update(float deltaTime)
{
	m_FixedTimeStep = deltaTime;
	m_DeltaTime = deltaTime * m_TimeMultiplier;
	m_FPSTimer += deltaTime;
	m_FramesPassed++;

	if (m_FPSTimer >= 1.f)
	{
		m_FPS = m_FramesPassed;
		m_FramesPassed = 0;
		m_FPSTimer -= 1.f;
	}
}

float Law::Time::GetDeltaTime()
{
	return m_DeltaTime;
}

const float Law::Time::GetFixedTimeStep()
{
	return m_FixedTimeStep;
}

int Law::Time::GetFPS()
{
	return m_FPS;
}

const float Law::Time::GetMaxTimePerFrame()
{
	return 1.f / m_MaxFramesPerSec;
}

void Law::Time::SetTimeMultiplier(float multiplier)
{
	m_TimeMultiplier = multiplier;
}
