#include "MiniginPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2D.h"

void Law::Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (m_Renderer == nullptr) 
	{
		Logger::LogError("Renderer::Init >> Renderer error");
		throw std::runtime_error(std::string("Renderer::Init >> SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void Law::Renderer::Render() const
{
	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void Law::Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void Law::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}
void Law::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y, const float width, const float height) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	dst.w = static_cast<int>(width);
	dst.h = static_cast<int>(height);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void Law::Renderer::RenderTexture(const Texture2D& texture, float dx, float dy, float dwidth, float dheight, SDL_Rect* pSrc) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(dx);
	dst.y = static_cast<int>(dy);
	dst.w = static_cast<int>(dwidth);
	dst.h = static_cast<int>(dheight);

	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), pSrc, &dst);
}

void Law::Renderer::DrawCircle(int xPos, int yPos, int radius)
{
	const int32_t diameter = (radius * 2);

	int32_t x = (radius - 1);
	int32_t y = 0;
	int32_t tx = 1;
	int32_t ty = 1;
	int32_t error = (tx - diameter);

	while (x >= y)
	{
		SDL_SetRenderDrawColor(m_Renderer, 255, 0, 0, 255);
		//  Each of the following renders an octant of the circle
		SDL_RenderDrawPoint(m_Renderer, xPos + x, yPos - y);
		SDL_RenderDrawPoint(m_Renderer, xPos + x, yPos + y);
		SDL_RenderDrawPoint(m_Renderer, xPos - x, yPos - y);
		SDL_RenderDrawPoint(m_Renderer, xPos - x, yPos + y);
		SDL_RenderDrawPoint(m_Renderer, xPos + y, yPos - x);
		SDL_RenderDrawPoint(m_Renderer, xPos + y, yPos + x);
		SDL_RenderDrawPoint(m_Renderer, xPos - y, yPos - x);
		SDL_RenderDrawPoint(m_Renderer, xPos - y, yPos + x);
		SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 255);

		if (error <= 0)
		{
			++y;
			error += ty;
			ty += 2;
		}

		if (error > 0)
		{
			--x;
			tx += 2;
			error += (tx - diameter);
		}
	}
}

void Law::Renderer::DrawRect(int xPos, int yPos, int width, int height)
{
	SDL_Rect rect;
	rect.x = xPos;
	rect.y = yPos;
	rect.w = width;
	rect.h = height;

	SDL_SetRenderDrawColor(m_Renderer, 255, 0, 0, 150);
	SDL_RenderDrawRect(m_Renderer, &rect);
	SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 255);
}
