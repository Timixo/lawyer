#pragma once
#include "Component.h"
#include "Helpers.h"
#include <Box2D.h>

namespace Law
{
	enum class BodyShape
	{
		RECTANGLE = 0,
		CIRCLE = 1
	};

	class PhysicsComponent final : public Component
	{
	public:
		PhysicsComponent(b2Vec2 position, b2Vec2 dimensions, uint16 categoryBits, uint16 maskBits, BodyShape shape = BodyShape::RECTANGLE, bool isStatic = true, bool isSubjectGravity = false, bool isSensor = false, float density = 1.f, float friction = 1.f);
		virtual ~PhysicsComponent();

		virtual void Update() override;
		virtual void Render() const override;

		b2Body* GetBody() const;
		static float GetPPM();

		static void SetPPM(float ppm);

		void SetStatic();
		void SetDynamic();
		void SetKinematic();
		void SetPosition(Point2f pos);
		void SetSubjectGravity(bool subject);
		void SetLinearVelocity(b2Vec2 linVel);

		void AddFixture(b2Vec2 position, b2Vec2 dimensions, uint16 categoryBits, uint16 maskBits, BodyShape shape = BodyShape::CIRCLE, bool isSensor = true);

	private:
		bool m_IsStatic;
		bool m_IsSubjectGravity;
		bool m_HasOwnLinVel;
		static float m_PPM;

		b2Vec2 m_Position;
		b2Vec2 m_Dimensions;

		b2Body* m_pBody;
	};
}