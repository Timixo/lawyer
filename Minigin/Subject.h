#pragma once
#include "Helpers.h"

namespace Law
{
	class Observer;
	class Subject
	{
	public:
		Subject();
		virtual ~Subject();

		void AddObserver(Observer* pObserver);
		void DeleteObserver(Observer* pObserver);

		void Notify(Event event);

	private:
		Observer* m_pHead;
	};
}