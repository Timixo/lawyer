#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "PhysicsManager.h"
#include "AudioManager.h"
#include "Scene.h"
#include "Time.h"
#include <SDL.h>
#include <thread>

using namespace std;
using namespace std::chrono;

SDL_Window* Law::Minigin::m_Window{ nullptr };
Law::Point2i Law::Minigin::m_WindowDim{ 640, 480 };

void Law::Minigin::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		Logger::LogError("Minigin::Initialize >> SDL initialization failed");
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	if (!m_Window)
	{
		m_Window = SDL_CreateWindow(
			"Programming 4 assignment",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			m_WindowDim.x,
			m_WindowDim.y,
			SDL_WINDOW_OPENGL
		);
	}
	if (m_Window == nullptr) 
	{
		Logger::LogError("Minigin::Initialize >> SDL window creation failed");
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(m_Window);
	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Resources/");
	PhysicsManager::GetInstance().Initialize();
	AudioManager::GetInstance().Initialize();

	SceneManager::GetInstance().Initialize();
}

void Law::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	SDL_Quit();
}

void Law::Minigin::Run()
{
	auto& renderer = Renderer::GetInstance();
	auto& sceneManager = SceneManager::GetInstance();
	auto& input = InputManager::GetInstance();

	float msPerFrame{ Time::GetMaxTimePerFrame() };
	auto lastTime = std::chrono::high_resolution_clock::now();
	float delay = 0.0f;
	bool doContinue = true;
	
	while (doContinue)
	{
		const auto currentTime = std::chrono::high_resolution_clock::now(); 
		float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count(); 
		lastTime = currentTime; 
		delay += deltaTime;
		while (delay >= msPerFrame)
		{ 
			doContinue = input.ProcessInput();
			Time::Update(msPerFrame);
			sceneManager.PhysicsUpdate();
			Law::PhysicsManager::GetInstance().Update(msPerFrame);
			sceneManager.Update();
			renderer.Render();
			delay = 0;
		} 
	}

	Cleanup();
}

SDL_Window* Law::Minigin::GetWindow()
{
	return m_Window;
}

Law::Point2i Law::Minigin::GetWindowDim()
{
	return m_WindowDim;
}
