#include "MiniginPCH.h"
#include "InputManager.h"
#include "Minigin.h"
#include "Command.h"
#include <SDL.h>
#include <SDL_syswm.h>
#include <thread>
#include <vector>

Law::InputManager::~InputManager()
{
	for (std::map<int, InputStruct>::iterator it{m_Inputs.begin()}; it != m_Inputs.end(); it++)
	{
		delete it->second.pCommand;
	}
}

bool Law::InputManager::ProcessInput()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
	}

	auto keyLambda = [this](int startIdx, int idxMultiplier)
	{
		int index = startIdx * idxMultiplier;
		int stopIndex = index + 16;

		for (; index < stopIndex; index++)
		{
			m_CurrentKeyboardState[index] = GetAsyncKeyState(index);
		}
	};

	memcpy(&m_OldKeyboardState, &m_CurrentKeyboardState, sizeof(short[256]));

	for (int i{}; i < 16; i++)
	{
		m_InputThreads[i] = std::async(std::launch::async, keyLambda, i, 16);
	}

	for (int i{}; i < XUSER_MAX_COUNT; i++)
	{
		m_OldGamepadState[i] = m_CurrentGamepadState[i];

		ZeroMemory(&m_CurrentGamepadState[i], sizeof(XINPUT_STATE));
		XInputGetState(i, &m_CurrentGamepadState[i]);
	}

	m_OldMousePosition = m_CurrentMousePosition;
	if (GetCursorPos(&m_CurrentMousePosition))
	{
		SDL_SysWMinfo wmInfo{};
		SDL_GetVersion(&wmInfo.version);
		SDL_GetWindowWMInfo(Minigin::GetWindow(), &wmInfo);
		HWND window{ wmInfo.info.win.window };

		ScreenToClient(window, &m_CurrentMousePosition);
	}

	m_MouseMovement.x = m_CurrentMousePosition.x - m_OldMousePosition.x;
	m_MouseMovement.y = m_CurrentMousePosition.y - m_OldMousePosition.y;

	for (int i{}; i < 16; i++)
	{
		m_InputThreads[i].get();
	}

	for (const std::pair<int, InputStruct>& inputPair : m_Inputs)
	{
		InputStruct input{ inputPair.second };
		InputTriggerState triggerState{ input.TriggerState };

		switch (triggerState)
		{
		case Law::InputTriggerState::PRESSED:
			// KEYBOARD
			if (IsKeyboardButtonDown(input.KeyboardCode) && !IsKeyboardButtonDown(input.KeyboardCode, true))
				input.pCommand->Execute(input.PlayerIndex);

			// GAMEPAD
			if (IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex) && !IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex, true))
				input.pCommand->Execute(input.PlayerIndex);

			break;
		case Law::InputTriggerState::RELEASED:
			// KEYBOARD
			if (!IsKeyboardButtonDown(input.KeyboardCode) && IsKeyboardButtonDown(input.KeyboardCode, true))
				input.pCommand->Execute(input.PlayerIndex);

			// GAMEPAD
			if (!IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex) && IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex, true))
				input.pCommand->Execute(input.PlayerIndex);

			break;
		case Law::InputTriggerState::DOWN:
			// KEYBOARD
			if (IsKeyboardButtonDown(input.KeyboardCode) && IsKeyboardButtonDown(input.KeyboardCode, true))
				input.pCommand->Execute(input.PlayerIndex);

			// GAMEPAD
			if (IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex) && IsGamepadButtonDown(input.GamepadButtonCode, input.PlayerGamepadIndex, true))
				input.pCommand->Execute(input.PlayerIndex);

			break;
		}
	}

	return true;
}

bool Law::InputManager::AddInput(const InputStruct& input)
{
	if (m_Inputs.find(input.Id) == m_Inputs.cend())
	{
		m_Inputs.insert({ input.Id, input });
		return true;
	}

	return false;
}

bool Law::InputManager::IsKeyboardButtonDown(int key, bool previousframe)
{
	if (!m_CurrentKeyboardState || !m_OldKeyboardState)
	{
		Logger::LogError("InputManager::IsKeyboardButtonDown >> Uninitialized keyboardstates");
		throw std::runtime_error("InputManager::IsKeyboardButtonDown >> Uninitialized keyboardstates");
	}

	if (key > 0x07 && key <= 0xFE)
	{
		if (previousframe)
			return m_OldKeyboardState[key] != 0;
		else
			return m_CurrentKeyboardState[key] != 0;
	}

	return false;
}

bool Law::InputManager::IsGamepadButtonDown(WORD button, GamepadIndex gamepadIndex, bool previousframe)
{
	if (button > 0x0000 && button <= 0x8000)
	{
		UINT playerIndex = static_cast<UINT>(gamepadIndex);

		if (previousframe)
			return (m_OldGamepadState[playerIndex].Gamepad.wButtons & button);
		return (m_CurrentGamepadState[playerIndex].Gamepad.wButtons & button);
	}

	return false;
}

Law::InputManager::InputManager()
{
	for (int i{}; i < 16; i++)
	{
		m_InputThreads.push_back(std::future<void>());
	}
}