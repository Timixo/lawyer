#pragma once

namespace Law
{
	class Component;

	class Command
	{
	public:
		virtual ~Command() {}
		virtual void Execute(int playerIdx) = 0;
		virtual void AddPlayer(int playerIdx, Component* component) = 0;
	};
}