#include "MiniginPCH.h"
#include "TextureComponent.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "Texture2D.h"
#include <SDL.h>

Law::TextureComponent::~TextureComponent()
{
	delete m_pSourceRect;
}

void Law::TextureComponent::Update()
{
}

void Law::TextureComponent::Render() const
{
	if (m_pTexture != nullptr)
	{
		if (m_IsShowing)
		{
			const auto pos{ GetGameObject()->GetTransform()->GetPosition() };

			float dest[4]{};
			dest[0] = pos.x;
			dest[1] = pos.y;

			if (m_HasCustomDimensions)
			{
				dest[2] = (float)m_Dimension.x;
				dest[3] = (float)m_Dimension.y;
			}
			else
			{
				dest[2] = m_Dimension.x * m_Multiplier;
				dest[3] = m_Dimension.y * m_Multiplier;
			}

			if (m_pSourceRect)
			{
				Renderer::GetInstance().RenderTexture(*m_pTexture, dest[0], dest[1], dest[2], dest[3], m_pSourceRect);
			}
			else
			{
				Renderer::GetInstance().RenderTexture(*m_pTexture, dest[0], dest[1], dest[2], dest[3]);
			}
		}
	}
	else
		Logger::LogWarning("TextureComponent::Render >> Trying to render texture without texture");
}

void Law::TextureComponent::SetTexture(const std::string& fileName, Texture texture, float multiplier)
{
	m_HasCustomDimensions = false;
	if (m_pTexture)
	{
		delete m_pTexture;
		m_pTexture = nullptr;
	}
	m_pTexture = ResourceManager::GetInstance().LoadTexture(fileName, texture);
	SDL_QueryTexture(m_pTexture->GetSDLTexture(), NULL, NULL, &m_Dimension.x, &m_Dimension.y);
	m_Multiplier = multiplier;
}

void Law::TextureComponent::SetSource(Point2f pos, Point2f dim)
{
	if (!m_pSourceRect)
		m_pSourceRect = new SDL_Rect();
	
	m_pSourceRect->x = static_cast<int>(pos.x);
	m_pSourceRect->y = static_cast<int>(pos.y);
	m_pSourceRect->w = static_cast<int>(dim.x);
	m_pSourceRect->h = static_cast<int>(dim.y);
}

void Law::TextureComponent::SetDimensions(Point2i dim)
{
	m_HasCustomDimensions = true;
	m_Dimension.x = dim.x;
	m_Dimension.y = dim.y;
}

void Law::TextureComponent::SetShow(bool show)
{
	m_IsShowing = show;
}

Law::Point2i Law::TextureComponent::GetDimension() const
{
	return m_Dimension;
}
