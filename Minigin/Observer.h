#pragma once
#include "Helpers.h"

namespace Law
{
	class Subject;
	class Observer
	{
	public:
		Observer();
		virtual ~Observer();
		virtual void OnNotify(Event event) = 0;

	protected:
		friend class Subject;
		Observer* m_pNext;

		Subject* m_pSubject;
	};
}