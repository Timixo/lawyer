#include "MiniginPCH.h"
#include <SDL.h>
#include <SDL_ttf.h>

#include "TextComponent.h"
#include "Renderer.h"
#include "Font.h"
#include "Texture2D.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "ResourceManager.h"

Law::TextComponent::~TextComponent()
{
	delete m_pFont;
	delete m_pTexture;
}

void Law::TextComponent::Update()
{
	if (m_NeedsUpdate)
	{
		if (m_pTexture)
		{
			delete m_pTexture;
			m_pTexture = nullptr;
		}

		const SDL_Color color = { 255,255,255 }; // only white text is supported now
		const auto surf = TTF_RenderText_Blended(m_pFont->GetFont(), m_Text.c_str(), color);
		if (surf == nullptr) 
		{
			Logger::LogError("TextComponent::Update >> Render text failed");
			throw std::runtime_error(std::string("TextComponent::Update >> Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr) 
		{
			Logger::LogError("TextComponent::Update >> Create text texture from surface failed");
			throw std::runtime_error(std::string("TextComponent::Update >> Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_pTexture = new Texture2D(texture);
		m_NeedsUpdate = false;
	}
}

void Law::TextComponent::Render() const
{
	if (m_pTexture != nullptr)
	{
		const auto pos{ GetGameObject()->GetTransform()->GetPosition() };
		Renderer::GetInstance().RenderTexture(*m_pTexture, pos.x, pos.y);
	}
	else
		Logger::LogWarning("TextComponent::Render >> Trying to render text without texture normally made in update");
}

// This implementation uses the "dirty flag" pattern
void Law::TextComponent::SetText(const std::string& text)
{
	m_Text = text;
	m_NeedsUpdate = true;
}

void Law::TextComponent::SetFont(const std::string& fontPath, unsigned int size)
{
	if (m_pFont)
	{
		delete m_pFont;
		m_pFont = nullptr;
	}
	m_pFont = ResourceManager::GetInstance().LoadFont(fontPath, size);
}
