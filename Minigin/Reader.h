#pragma once
#include "Singleton.h"

namespace Law
{
	class Reader final : public Singleton<Reader>
	{
	public:
		void Open(const std::string& file);
		void Close();

		std::string ReadLine();

	private:
		bool m_IsOpen;
		std::ifstream* m_pReader;
	};
}