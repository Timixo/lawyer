#include "MiniginPCH.h"
#include "Subject.h"
#include "Observer.h"
#include <algorithm>

Law::Subject::Subject()
	: m_pHead{nullptr}
{
}

Law::Subject::~Subject()
{
	while (m_pHead)
	{
		m_pHead->m_pSubject = nullptr;
		m_pHead = m_pHead->m_pNext;
	}
}

void Law::Subject::AddObserver(Observer* pObserver)
{
	pObserver->m_pNext = m_pHead;
	m_pHead = pObserver;
}

void Law::Subject::DeleteObserver(Observer* pObserver)
{
	if (m_pHead == pObserver)
	{
		m_pHead = pObserver->m_pNext;
		pObserver->m_pNext = nullptr;
		return;
	}

	Observer* pCurrent = m_pHead;
	while (pCurrent)
	{
		if (pCurrent->m_pNext == pObserver)
		{
			pCurrent->m_pNext = pObserver->m_pNext;
			pObserver->m_pNext = nullptr;
			return;
		}
		pCurrent = pCurrent->m_pNext;
	}
}

void Law::Subject::Notify(Event event)
{
	Observer* pCurrent = m_pHead;

	while (pCurrent)
	{
		pCurrent->OnNotify(event);
		pCurrent = pCurrent->m_pNext;
	}
}
