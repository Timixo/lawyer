#pragma once
#include "SceneManager.h"
#include "Time.h"
#include <string>

namespace Law
{
	class GameObject;
	class Scene
	{
	public:
		void Add(GameObject* object);

		void RootPhysicsUpdate();
		void RootUpdate();
		void RootRender() const;

		virtual void Initialize() = 0;
		virtual void PhysicsUpdate() = 0;
		virtual void Update() = 0;
		virtual void Render() const = 0;

		virtual ~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

	protected:
		explicit Scene(const std::string& name);

	private: 
		std::string m_Name;
		std::vector <GameObject*> m_pObjects{};

		static unsigned int m_IdCounter; 
	};

}
