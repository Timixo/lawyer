#pragma once
#include "TextureComponent.h"
#include "Helpers.h"
#include <unordered_map>

namespace Law
{
	class SpriteComponent final : public TextureComponent
	{
	public:
		SpriteComponent();

		virtual void Update() override;
		virtual void Render() const override;

		void Play();
		void Pause();

		void SetSpriteSheet(const std::string& sheet, Texture texture, const SpriteSheetInfo& info, float multiplier);
		void SetSpriteState(SpriteState state);
		void SetSpriteCol(int col);
		void SetFrameTime(float frameTime);
		void SetLooping(bool looping, bool stopAfterLoop);

		bool IsPaused();

	private:
		bool m_IsPaused;
		bool m_IsLooping;
		bool m_StopAfterLoop;

		int m_CurrentRow;
		int m_CurrentColumn;

		float m_PassedTime;
		float m_FrameTime;
		static float m_StandardFrameTime;

		Point2i m_SpriteDim;

		SpriteState m_CurrentState;

		SpriteSheetInfo m_Info;
	};
}