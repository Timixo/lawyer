#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#pragma warning(pop)
#include "Component.h"

namespace Law
{
	class TransformComponent final : public Component
	{
	public:
		const glm::vec3& GetPosition() const { return m_Position; }
		void SetPosition(float x, float y, float z);

		virtual void Update() {};
		virtual void Render() const {};
	private:
		glm::vec3 m_Position;
	};
}
