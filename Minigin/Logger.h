#pragma once
#include <string>

namespace Law
{
	class Logger final
	{
	public:
		enum class LogLevel
		{
			Info = 0,
			Warning = 1,
			Error = 2
		};

		static void LogInfo(const std::string& message);
		static void LogWarning(const std::string& message);
		static void LogError(const std::string& message);

	private:
		Logger() = default;

		static void Log(LogLevel level, const std::string& message);
	};
}