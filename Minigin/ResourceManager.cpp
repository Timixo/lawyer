#include "MiniginPCH.h"
#include "ResourceManager.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "Renderer.h"
#include "Texture2D.h"
#include "Font.h"

Law::ResourceManager::~ResourceManager()
{
	for (std::map<Texture, Texture2D*>::iterator it{ m_pTextures.begin() }; it != m_pTextures.end(); it++)
	{
		delete it->second;
	}
}

void Law::ResourceManager::Init(const std::string& dataPath)
{
	m_DataPath = dataPath;

	// load support for png and jpg, this takes a while!

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) 
	{
		Logger::LogError("ResourceManager::Init >> Failed to load support for png's");
		throw std::runtime_error(std::string("ResourceManager::Init >> Failed to load support for png's: ") + SDL_GetError());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) 
	{
		Logger::LogError("ResourceManager::Init >> Failed to load support for jpg's");
		throw std::runtime_error(std::string("ResourceManager::Init >> Failed to load support for jpg's: ") + SDL_GetError());
	}

	if (TTF_Init() != 0) 
	{
		Logger::LogError("ResourceManager::Init >> Failed to load support for fonts");
		throw std::runtime_error(std::string("ResourceManager::Init >> Failed to load support for fonts: ") + SDL_GetError());
	}
}

Law::Texture2D* Law::ResourceManager::LoadTexture(const std::string& file, Texture texture)
{
	if (m_pTextures.find(texture) != m_pTextures.end())
	{
		return m_pTextures[texture];
	}

	const auto fullPath = m_DataPath + file;
	auto SDLTexture = IMG_LoadTexture(Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
	if (SDLTexture == nullptr)
	{
		Logger::LogError("ResourceManager::LoadTexture >> Failed to load texture");
		throw std::runtime_error(std::string("ResourceManager::LoadTexture >> Failed to load texture: ") + SDL_GetError());
	}
	m_pTextures[texture] = new Texture2D(SDLTexture);
	return m_pTextures[texture];
}

Law::Font* Law::ResourceManager::LoadFont(const std::string& file, unsigned int size) const
{
	return new Font(m_DataPath + file, size);
}

const std::string& Law::ResourceManager::GetDataPath() const
{
	return m_DataPath;
}
