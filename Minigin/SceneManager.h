#pragma once
#include "Singleton.h"
#include <vector>

namespace Law
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		void AddScene(Scene* pScene);

		~SceneManager();

		void Initialize();
		void PhysicsUpdate();
		void Update();
		void Render();
	private:
		friend class Singleton<SceneManager>;
		SceneManager() = default;
		std::vector<Scene*> m_pScenes;
	};
}
